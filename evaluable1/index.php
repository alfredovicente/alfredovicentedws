<?php
    session_start();
    if(!$_SESSION["idCliente"]){
            header("Location: login.php");
    }
?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>

<body>
    <h1>Inicio</h1>
    <h2>Menu</h2>
    <div>
        <ol>
            <li><a href="articulos.php">Artículos</a></li>
            <li><a href="clientes.php">Clientes</a></li>
            <li><a href="pedidos.php">Pedidos</a></li>
            <li><a href="control.php?action=cerrar">Cerrar sesion</a></li>
        </ol>
        <ul>
            <li><a href="DWST03Evaluable1.pdf">Enunciado práctica profesor</a></li>
            <li><a href="documentacion.php">Documentación alumno</a></li>
            <li><a href="https://bitbucket.org/alfredovicente/alfredovicentedws/src/master/">Bitbucket alumno</a></li>
            <li><a href="https://alfredovicentedws.herokuapp.com/">Heroku alumno</a></li>
        </ul>
    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
