<?php

function getDB() {
    $DBserver="localhost"; 
    $DBname="eva"; 
    $DBuser="root";  
    $DBpassword="";
    $dbConnection = new PDO("mysql:host=$DBserver;dbname=$DBname", $DBuser, $DBpassword);
	$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbConnection;
}

function consulta ($sql,$values){
	try {
		$db = getDB();
		$query = $db->prepare($sql);
		$query->execute($values);
		$ret = $query->fetchAll();
		if($ret) {
			return $ret;
			$db = null;
		} else {
			throw new PDOException('La consulta no té resultats');
		}
	
	} catch(PDOException $e) {
		$error['error'] = $e->getMessage();
		return $error;
	}
}

function consulta_update ($sql,$values){
	try {
		$db = getDB();
		$query = $db->prepare($sql);
		$query->execute($values);
	
	} catch(PDOException $e) {
		$error['error'] = $e->getMessage();
		return $error;
	}
}

function consulta_sin_val ($sql) {
	try {
		$db = getDB();
		$query = $db->prepare($sql);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	} catch(PDOException $e) {
		$error['error'] = $e->getMessage();
		return $error;
	}
}

?>