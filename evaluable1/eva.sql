-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Temps de generació: 30-10-2019 a les 17:13:53
-- Versió del servidor: 10.4.8-MariaDB
-- Versió de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `eva`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `Articulo`
--

CREATE TABLE `Articulo` (
  `IdArticulo` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Precio` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Bolcament de dades per a la taula `Articulo`
--

INSERT INTO `Articulo` (`IdArticulo`, `Nombre`, `Precio`) VALUES
(1, 'cosa4', 2.6),
(2, 'Sombrero5', 4.5),
(3, 'JOSE', 4.7),
(4, 'VANESSA', 4);

-- --------------------------------------------------------

--
-- Estructura de la taula `Cliente`
--

CREATE TABLE `Cliente` (
  `idCliente` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Email` varchar(45) NOT NULL,
  `Usuari` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `FechaNacimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Bolcament de dades per a la taula `Cliente`
--

INSERT INTO `Cliente` (`idCliente`, `Nombre`, `Email`, `Usuari`, `Password`, `FechaNacimiento`) VALUES
(1, 'Alfredo', 'alviboi@gmail.com', 'aliboi', 'asdsad', '2019-10-15'),
(2, 'cxz', 'cxzxc@dadsa.esd', 'asdd', 'asda', '2019-10-29'),
(3, 'aa', 'aa', 'aaa', '', '2019-10-15'),
(4, 'aa', 'aa', 'aaa', '', '2019-10-15'),
(5, 'aa', 'aa', 'aaa', '', '2019-10-15'),
(6, 'aa', 'alviboi@gmail.com', 'lliurex', 'lliurex', '2019-10-08');

-- --------------------------------------------------------

--
-- Estructura de la taula `Linea`
--

CREATE TABLE `Linea` (
  `idLinea` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Pedido_idPedido` int(11) NOT NULL,
  `Pedido_idCliente` int(11) NOT NULL,
  `Pedido_Articulo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de la taula `Pedido`
--

CREATE TABLE `Pedido` (
  `idPedido` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `Articulo`
--
ALTER TABLE `Articulo`
  ADD PRIMARY KEY (`IdArticulo`);

--
-- Índexs per a la taula `Cliente`
--
ALTER TABLE `Cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Índexs per a la taula `Linea`
--
ALTER TABLE `Linea`
  ADD PRIMARY KEY (`idLinea`),
  ADD KEY `articulos` (`Pedido_Articulo`),
  ADD KEY `cliente` (`Pedido_idCliente`),
  ADD KEY `pedido` (`Pedido_idPedido`);

--
-- Índexs per a la taula `Pedido`
--
ALTER TABLE `Pedido`
  ADD PRIMARY KEY (`idPedido`),
  ADD KEY `cliente2` (`Cliente_idCliente`);

--
-- Restriccions per a les taules bolcades
--

--
-- Restriccions per a la taula `Linea`
--
ALTER TABLE `Linea`
  ADD CONSTRAINT `articulos` FOREIGN KEY (`Pedido_Articulo`) REFERENCES `Articulo` (`IdArticulo`),
  ADD CONSTRAINT `cliente` FOREIGN KEY (`Pedido_idCliente`) REFERENCES `Cliente` (`idCliente`),
  ADD CONSTRAINT `pedido` FOREIGN KEY (`Pedido_idPedido`) REFERENCES `Pedido` (`idPedido`);

--
-- Restriccions per a la taula `Pedido`
--
ALTER TABLE `Pedido`
  ADD CONSTRAINT `cliente2` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `Cliente` (`idCliente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
