<?php
require('bd.php');
//if(!session_id()) session_start();
//var_dump($_POST);
//exit();
if (isset($_GET["action"])){
	$action=$_GET["action"];
} elseif (isset($_POST["action"])) {
	$action=$_POST["action"];
} else {
	$error['error']="Falta el parámetro de action";
    header("Location: nuevo.php?error=$error[error]");
}


switch ($action) {
  case 'borrar':
    $val = array($_GET["id"]);
    $sql_borrar = "DELETE FROM Articulo WHERE IdArticulo=?";
    $error = consulta($sql_borrar,$val);
    if ($error){
		header("Location: error.php?error=$error[error]");
	} else {
		header('Location: articulos.php');
	}    
    break;
  case 'compra':
	unset($_POST['action']);
		$text = "";
		foreach ($_POST as $key => $value) {
                        $text = $text.$key."=".$value."&";
                    }
		header("Location: pedidos.php?$text"."comprado=si");
    break;
  case 'cerrar':
  	session_start();
  	session_destroy ();
  	header("Location: login.php");
  	break;
  case 'actualizar':
  	$val = array($_GET["nombre"],$_GET["precio"],$_GET["idArticulo"]);
  	$sql = "UPDATE Articulo SET Nombre=?, Precio=? WHERE IdArticulo=?";
	$error = consulta_update($sql,$val);
	if ($error){
		header("Location: error.php?error=$error[error]");
	} else {
		header("Location: actualizar.php?idArticulo=$_GET[idArticulo]&nombre=$_GET[nombre]&precio=$_GET[precio]&actualizado=si");
	}
    break;
  
  case 'nuevo':
  	$val = array($_GET["idArticulo"],$_GET["nombre"],$_GET["precio"]);
  	$sql = "INSERT INTO Articulo (idArticulo,Nombre,Precio) VALUES (?,?,?)";
	$error = consulta_update($sql,$val);

	if ($error){
		header("Location: nuevo.php?error=$error[error]");
	} else {
		header("Location: nuevo.php?anadido=si");
	}
    break;

  case 'actualizar_clientes':
  	$val=array($_POST['idCliente'],$_POST['Nombre'],$_POST['Email'],$_POST['FechaNacimiento'],$_POST['Usuari'],$_POST['idCliente']);
  	$sql = "UPDATE Cliente SET idCliente=?, Nombre=?, Email=?, FechaNacimiento=?, Usuari=? WHERE idCliente=?";  		
	$error = consulta_update($sql,$val);
	if ($error){
		header("Location: error.php?error=$error[error]");
	} else {
		unset($_POST['action']);
		$text = "";
		foreach ($_POST as $key => $value) {
                        $text = $text.$key."=".$value."&";
                    }
		header("Location: actualizar_clientes.php?$text"."actualizado=si");
	}
    break;
  case 'borrar_clientes':
    $val = array($_GET["idCliente"]);
    $sql_borrar = "DELETE FROM Cliente WHERE idCliente=?";
    $error = consulta($sql_borrar,$val);
	header('Location: clientes.php');
   
    break;

  case 'nuevo_clientes':
  	//var_dump($_POST);
  	unset($_POST['action']);
  	$text=array();
  	$val=array();
  	foreach ($_POST as $key => $value) {
  		array_push($text, $key);
  		array_push($val, $value);
  	}  	
  	//var_dump($val);
  	//var_dump($text);
  	$sql = "INSERT INTO Cliente (".implode(', ', $text).") VALUES (?,?,?,?,?,?)";
  	//echo $sql;
	$error = consulta_update($sql,$val);
	
	if ($error){
		header("Location: error.php?error=$error[error]");
	} else {
		header("Location: nuevo_cliente.php?anadido=si");
	}
    break;

  case 'login':
  	$username = $_POST['Usuario'];
	$password = $_POST['password'];  	
  	$sql = "SELECT password, idCliente FROM Cliente WHERE Usuari=?";
  	$val = array($_POST['Usuario']);
	$result = consulta($sql,$val);
	if ($result[0]['password'] == $password){
		session_start();
		$_SESSION["idCliente"] = $result[0]['idCliente'];
		//echo $_SESSION['idCliente'];
		header("Location: index.php");
	} else {
		$error[error]="Error en el inicio de sesion";
    	header("Location: error.php?error=$error[error]");
	}
	//var_dump($result);  
  	break;
  default:
  	$error['error']="Falta el parámetro de action o no esta bien definido";
    header("Location: error.php?error=$error[error]");
    break;
}
