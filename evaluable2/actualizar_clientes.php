<?php
/**
* @file actualizar_clientes.php
* @brief Archivo para actualizar Cliente
*
* Contiene formulario para actualizar un cliente.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>


<body>
    <h1>Actualizar Clientes</h1>
    <h2>Menu</h2>
    <div>
    		
            <a href="clientes.php">Volver</a>
            <br/><br/>
            <form action="control.php" method="post">
            	<table border='1'>
                    <?php
                    /**
                     * Recogemos todo el html en esta variable
                     */
                    $text = "";
                    /**
                     * Enfunción del tipo de de variable usamos un type
                     *
                     * @param [string] $val
                     * @return [string] Con el valor type
                     */
                    function tipo ($val) {
                        switch ($val) {
                            case 'FechaNacimiento': return 'date'; break;
                            case 'idCliente': return 'number'; break;
							case 'password': return 'password'; break;
                            default: return 'text'; break;      
                        }
                    }
					//Recorremos todo el array GET para crear nuestros inputs y anadir el valor.
                    foreach ($_GET as $key => $value) {
                        if ($key != 'actualizado'){
                            $tip = tipo($key);
                            echo "<tr><td>$key</td><td><input type='$tip' name='$key' value='$value'></td></tr>";
                            $text = $text.$key."=&";
                        }                        
                    }

                    ?>

            		<input type="text" name="action" value="actualizar_clientes" hidden>

            	</table>
            	<br>
            	<input type="submit" value="Actualizar">
	            <input type="button" value="Borrar" onclick="javascript:location.href='actualizar_clientes.php?<? echo $text ?>'">
            </form>
            <br>
            
            <div style='color: red'>
    			<?php
    			if(isset($_GET['actualizado'])){
					echo "El valor ha sido actualizado";
				}
				?>
    		</div>




    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
