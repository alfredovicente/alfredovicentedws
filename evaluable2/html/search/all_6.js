var searchData=
[
  ['getemail',['getEmail',['../class_cliente.html#a02a01849f28e2535e888ae4ec87b20f2',1,'Cliente']]],
  ['getfecha',['getFecha',['../class_cliente.html#a50078690f0773491fd4a7cbbca4e280f',1,'Cliente\getFecha()'],['../class_pedido.html#a50078690f0773491fd4a7cbbca4e280f',1,'Pedido\getFecha()']]],
  ['getid',['getId',['../classarticulos.html#a12251d0c022e9e21c137a105ff683f13',1,'articulos\getId()'],['../class_cliente.html#a12251d0c022e9e21c137a105ff683f13',1,'Cliente\getId()']]],
  ['getid_5farticulo',['getId_articulo',['../classlineas.html#a56cb7e817a3b390bae690ec19c16b89d',1,'lineas']]],
  ['getid_5fcliente',['getId_cliente',['../class_pedido.html#ac46f2007726c82e594e5e126e68e40d6',1,'Pedido']]],
  ['getid_5fpedido',['getId_pedido',['../classlineas.html#a357c185dab809a9af2701f5cb22a90e3',1,'lineas\getId_pedido()'],['../class_pedido.html#a357c185dab809a9af2701f5cb22a90e3',1,'Pedido\getId_pedido()']]],
  ['getnombre',['getNombre',['../classarticulos.html#a19ecce3d19e108b3bdfb5c8e74512062',1,'articulos\getNombre()'],['../class_cliente.html#a19ecce3d19e108b3bdfb5c8e74512062',1,'Cliente\getNombre()'],['../classlineas.html#a19ecce3d19e108b3bdfb5c8e74512062',1,'lineas\getNombre()']]],
  ['getnum_5flinea',['getNum_linea',['../classlineas.html#a465ce1ae629d9bfecc65f799187ca417',1,'lineas']]],
  ['getpassword',['getPassword',['../class_cliente.html#a04e0957baeb7acde9c0c86556da2d43f',1,'Cliente']]],
  ['getprecio',['getPrecio',['../classarticulos.html#ae1f5d7a6ecf229ebc209b561477ae5ec',1,'articulos\getPrecio()'],['../classlineas.html#ae1f5d7a6ecf229ebc209b561477ae5ec',1,'lineas\getPrecio()']]],
  ['getusuario',['getUsuario',['../class_cliente.html#a38abe751ca21ffb95d9c90862e0fb96f',1,'Cliente']]]
];
