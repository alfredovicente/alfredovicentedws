<?php
/**
* @file clientes.php
* @brief Saca el listado de clientes
*
* Hace peticion para mostrar todos los clientes.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Clientes</title>
</head>
<?php

require("fichero.php");
/**
 * Recogemos todos los clientes
 */
$result = ClienteReadAll();
/**
 * Asignamos el prosimo id que vamos a necesitar
 */
$ind=max(comprobar_id_cliente())+1;

?>
<body>
    <h1>Clientes</h1>
    <h2>Menu</h2>
    <div>

            <a href="index2.php">Volver</a><br>
            <a href="nuevo_cliente.php?id=<?php echo $ind ?>">Nuevo</a><br/><br/>

<?php

echo "<table border='1'>";
echo "<tr><td>Id</td><td>Nombre</td><td>Email</td><td>Fecha de Nacimiento</td><td>Usuari</td><td>Ver</td><td>Actualizar</td><td>Borrar</td></tr>";
foreach ($result as $value) {
    echo "<tr>"; 
    echo "<td>".$value->getId()."</td><td>".$value->getNombre()."</td><td>".$value->getEmail()."</td><td>".$value->getFecha()."</td><td>".$value->getUsuario()."</td><td><a href='ver_clientes.php?id=".$value->getId()."'>Ver</a>
</td><td><a href='actualizar_clientes.php?idCliente=".$value->getId()."&Nombre=".$value->getNombre()."&Email=".$value->getEmail()."&FechaNacimiento=".$value->getFecha()."&Usuari=".$value->getUsuario()."&password=".$value->getPassword()."'>Actualizar</a></td><td><a href='control.php?action=borrar_clientes&idCliente=".$value->getId()."'>Borrar</a>
</td>";
    echo "</tr>";
}
echo "</table>";



?>


    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
