<?php
/**
* @file ver_clientes.php
* @brief Archivo para ver todos los cliente
*
* Se crea una tabla para ver en detalle cada uno de los clientes
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Clientes</title>
</head>

<body>
    <h1>Ver Clientes</h1>
    <h2>Menu</h2>
    <div>

            <a href="clientes.php">Volver</a><br><br>

<?php

require("fichero.php");
/**
 * Leemos todos los datos de un determinado cliente.
 */
$value = ClienteRead($_GET['id']);
echo "<table border='1'>";
echo "<tr><td>key</td><td>valor</td></tr>";
//Creamos una tabla con todos los clientes. Nos mostrara todo el array, incluyendo el indice y el nombre.
    echo "<tr>"; 
    echo "<td>id</td><td>".$value->getId()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>nombre</td><td>".$value->getNombre()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>email</td><td>".$value->getEmail()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>fecha</td><td>".$value->getFecha()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>usuario</td><td>".$value->getUsuario()."</td>";
    echo "</tr>";


echo "</table>";



?>


    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
