<?php
/**
* @file articulos.php
* @brief Saca el listado de articulos
*
* Hace peticion para mostrar todos los articulos.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>

<body>
    <h1>Artículos</h1>
    <h2>Menu</h2>
    <div>

            <a href="index2.php">Volver</a><br>
            <a href="nuevo.php">Nuevo</a><br/><br/>

<?php

require_once("fichero.php");
/**
 * Recogemos todos los art'iculos
 */
$result =  ArticuloReadAll();
echo "<table border='1'>";
echo "<tr><td>Id</td><td>Nombre</td><td>Precio</td><td>Imagen</td><td>Ver</td><td>Actualizar</td><td>Borrar</td></tr>";
//Recorremos todos los articulos para crear una tabla con todos ellos.
foreach ($result as $value) {
    echo "<tr>";
    echo "<td>".$value->getId()."</td><td>".$value->getNombre()."</td><td>".$value->getPrecio()."</td><td><a href='img/".$value->getId().".png' target='_blank'>Imagen</a>
    </td><td><a href='ver.php?id=".$value->getId()."'>Ver</a>
</td><td><a href='actualizar.php?idArticulo=".$value->getId()."&nombre=".$value->getNombre()."&precio=".$value->getPrecio()."'>Actualizar</a>
</td><td><a href='control.php?action=borrar&id=".$value->getId()."'>Borrar</a>
</td>";
    echo "</tr>";
}
echo "</table>";



?>


    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
