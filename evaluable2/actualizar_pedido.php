<?php
/**
* @file actualizar_pedido.php
* @brief Archivo para actualizar los parámetros de un pedido
*
* Formulario que actualiza los parámetros del pedido
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/

/**
 * Parámetro recibido por método GET
 */
$id=$_GET['idPedido'];
/**
 * Parámetro recibido por método GET
 */
$cliente=$_GET['idCliente'];
/**
 * Parámetro recibido por método GET
 */
$fecha=$_GET['Fecha'];

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>


<body>
    <h1>Actualizar Pedidos</h1>
    <h2>Menu</h2>
    <div>
    		
            <a href="listado_pedidos.php">Volver</a>
            <br/><br/>
            <form action="control.php" method="post" enctype="multipart/form-data">
            	<table border='1'>
            		<tr>
            			<td>idPedido</td>
            			<td><input type="number" name="idPedido" value="<?php echo $id ?>" readonly></td>
            		</tr>
            		<tr>
            			<td>idCliente</td>
            			<td><input type="number" name="idCliente" value="<?php echo $cliente ?>"></td>
            		</tr>
            		<tr>
            			<td>Fecha</td>
            			<td><input type="date" name="fecha"" value="<?php echo $fecha ?>"></td>
					</tr>
            		<input type="text" name="action" value="actualizar_pedido" hidden>

            	</table>
            	<br>
            	<input type="submit" value="Actualizar">
	            <input type="button" value="Borrar" onclick="javascript:location.href='actualizar_pedido.php?idPedido=&idCliente=&fecha='">
				<input type="button" value="Actualizar Lineas del pedido" onclick="javascript:location.href='actualizar_lineas.php?idPedido=<?php echo $id ?>'">

			</form>
			<br>
			

            <div style='color: red'>
    			<?php
				/**
				 * Si se actualiza se pone un aviso
				 */
    			if(isset($_GET['actualizado'])){
					echo "El valor ha sido actualizado";
				}
				?>
    		</div>
	</div>
	




    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>