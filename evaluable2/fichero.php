<?php
/**
 * @file fichero.php
* @brief Archivo que gestiona el acceso a todos los ficheros
*
* Accede a todos los ficheros necesarios y saca los valores y escribe en ellos.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/

require("classes/articulo.php");
require("classes/cliente.php");
require("classes/linea.php");
require("classes/pedido.php");


/**
 * Esta función nos permite comprobar todos los id de un 
 * archivo para que no se repitan.
 *
 * @return [array] $ind Devuelve todos los índices que hay.
 */
function comprobar_id_art () {
    $arts = ArticuloReadAll();
    $ind = array();
    foreach ($arts as $value) {
        array_push($ind,$value->getId());
    }
    return $ind;
}
/**
 * Lee todos los artículos que hay en el archivo de artículos.
 *
 * @return array 
 */
function ArticuloReadAll() {
    $fp = fopen("articulos.csv", "r");
    $articulos = [];
    $linea = "";
    $linea_articulo = [];
    $i = 0;
    while (!feof($fp)){
        $linea_articulo = fgetcsv($fp);
        //$linea_articulo = explode(";",$linea);
        $articulos[$i] = new articulos ($linea_articulo[0],$linea_articulo[1],$linea_articulo[2]);
        $i++;
    }
    return $articulos;
    fclose($fp); 
}
/**
 * Crea un artículo a partir de un artículo dado
 *
 * @param [articulo] $Articulo Les pasamos un objeto
 * @return void
 */
function ArticuloCreate($Articulo) {
    $ind = comprobar_id_art ();
    $fp = fopen("articulos.csv", "a");
    
    if (in_array($Articulo -> getId(), $ind, true)) {
        $error['error']= "Se encontró indice repetido. El próximo id libre en el contador es ".(max($ind)+1).".\n";
		header("Location: error.php?error=$error[error]");
    } else {
        $arr = array($Articulo -> getId(), $Articulo -> getNombre(), $Articulo -> getPrecio());
        fputcsv($fp,$arr);
    }    
    fclose($fp);
}
/**
 * Leemos un artículo dada una id
 *
 * @param [string] $id
 * @return [object] $value
 */
function ArticuloRead($id){
    $Articulos = ArticuloReadAll(); 
    $fp = fopen("articulos.csv", "a");
    foreach ($Articulos as $value) {
        if ($value -> getId() == $id){
            return $value;
            break;
        }
    }
    return "No existe";
    fclose($fp);
}
/**
 * Actualizamos un articulo dado un objecto articulo
 *
 * @param [object] $Articulo
 * @return void
 */
function ArticuloUpdate($Articulo){
    $fp = fopen("articulos.csv", "r");
    $articulos = [];
    $linea = "";
    $linea_articulo = [];
    $i = 0;
    do {
        $linea_articulo = fgetcsv($fp);
        //var_dump($linea_articulo);
        //$linea_articulo = explode(";",$linea);
        if ($linea_articulo) $articulos[$i] = $linea_articulo;
        $i++;
    }  while (!feof($fp));
    //var_dump($articulos[0][0]);

    fclose($fp);
    for ($i=0; $i < sizeof($articulos); $i++) { 
        # code...
        if ($articulos[$i][0] == $Articulo -> getId()){
            $articulos[$i] = array($Articulo -> getId(), $Articulo -> getNombre(), $Articulo -> getPrecio());
            break;
        }
    }
    //var_dump($articulos[0][0]);
    $fp = fopen("articulos.csv", "w");
    for ($i=0; $i < sizeof($articulos); $i++) { 
        # code...
        
        //echo '<br>';
        $arr = $articulos[$i];
        //var_dump($arr);
        fputcsv($fp,$arr);
    }
    fclose($fp);
}
/**
 * Borrar articulo dado id
 *
 * @param [string] $id
 * @return void
 */
function ArticuloDelete($id){
    $fp = fopen("articulos.csv", "r");
    $articulos = [];
    $linea_articulo = [];
    $i = 0;
    do {
        $linea_articulo = fgetcsv($fp);
        if ($linea_articulo) $articulos[$i] = $linea_articulo;
        $i++;
    }  while (!feof($fp));
    fclose($fp);
    for ($i=0; $i < sizeof($articulos); $i++) { 
        # code...
        if ($articulos[$i][0] == $id){
            unset($articulos[$i]);
            break;
        }
    }
    $fp = fopen("articulos.csv", "w");
    foreach ($articulos as $value) {
        fputcsv($fp,$value);
    }
    fclose($fp);
}

/*
CLIENTES
*/

/**
 * Cogemos en un array todos los id de los clientes.
 *
 * @return [array] $ind
 */
function comprobar_id_cliente () {
    $clie = ClienteReadAll();
    $ind = [];
    foreach ($clie as $value) {
        array_push($ind,$value->getId());
    }
    return $ind;
}
/**
 * Con esta funcion leemos si el usuario está detro del archivo de usuarios
 *
 * @param [string] $usuario
 * @return [string]
 */
function comprobar_usuario ($usuario) {
    $clie = ClienteReadAll();
    $usr = [];
    foreach ($clie as $value) {
        array_push($usr,$value->getUsuario());
    }
    if (in_array($usuario, $usr)) {
        return "repe";
    } else {
        return "OK";
    }
    return $usr;
}
/**
 * Leemos todos los clientes
 *
 * @return [array] Todos los objetos clientes generados a partir del archivo de clientes.
 */
function ClienteReadAll() {
    $fp = fopen("clientes.csv", "r");
    $clientes = [];
    $linea = "";
    $linea_cliente = [];
    $i = 0;
    while (!feof($fp)){
        $linea_cliente = fgetcsv($fp);
        //$linea_Cliente = explode(";",$linea);
        $clientes[$i] = new cliente ($linea_cliente[0],$linea_cliente[1],$linea_cliente[2],$linea_cliente[3],$linea_cliente[4],$linea_cliente[5]);
        $i++;
    }
    unset($clientes[$i-1]);
    return $clientes;
    fclose($fp); 
}
/**
 * Crear un cliente a partir de un objeto cliente dado.
 *
 * @param [object] $Cliente
 * @return [string] Con mensajes de error o OK
 */
function ClienteCreate($Cliente) {
    $ind = comprobar_id_cliente ();
    $fp = fopen("clientes.csv", "a");    
    if (in_array($Cliente -> getId(), $ind, true) && comprobar_usuario($Cliente->getUsuario())=="repe") {
        return "Se encontró indice o usuario repetido.\n";
    } else {
        $arr = array ($Cliente -> getId(), $Cliente -> getNombre(), $Cliente -> getFecha(), $Cliente -> getEmail(), $Cliente -> getUsuario(), $Cliente -> getPassword());
        fputcsv($fp,$arr);
        return "OK";
    }    
    fclose($fp);
}
/**
 * Leemos un objeto cliente dado su id
 *
 * @param [string] $id
 * @return [string] Errores dados
 */
function ClienteRead($id){
    $Clientes = ClienteReadAll(); 
    $fp = fopen("clientes.csv", "a");
    foreach ($Clientes as $value) {
        if ($value -> getId() == $id){
            return $value;
            break;
        }
    }
    return "No existe. Probablemente lo has borrado antes";
    fclose($fp);
}
/**
 * Leer el usuario a partir del id del usuario
 *
 * @param [string] $usr Id del usuario del que queremos conocer el valor usuario
 * @return [string] $value->getUsuario();
 */
function ClienteReadUser($usr){
    $Clientes = ClienteReadAll(); 
    $fp = fopen("clientes.csv", "a");
    foreach ($Clientes as $value) {
        if ($value -> getId() == $usr){
            return $value->getUsuario();
            break;
        }
    }
    return "No existe";
    fclose($fp);
}
/**
 * Actualizar cliente a partir de un objeto cliente
 *
 * @param [object] $Cliente
 * @return [string] Errores en la función
 */
function ClienteUpdate($Cliente){
    if (comprobar_usuario($Cliente->getUsuario()=="OK")){
        $fp = fopen("clientes.csv", "r");
        $Clientes = [];
        $linea_Cliente = [];
        $i = 0;
        do {
            $linea_Cliente = fgetcsv($fp);
            if ($linea_Cliente) $Clientes[$i] = $linea_Cliente;
            $i++;
        }  while (!feof($fp));
    
        fclose($fp);
        for ($i=0; $i < sizeof($Clientes); $i++) { 
            if ($Clientes[$i][0] == $Cliente -> getId()){
                $Clientes[$i] = array($Cliente -> getId(), $Cliente -> getNombre(), $Cliente -> getEmail(), $Cliente -> getFecha(), $Cliente -> getUsuario(), $Cliente -> getPassword());
                break;
            }
        }
        $fp = fopen("clientes.csv", "w");
        for ($i=0; $i < sizeof($Clientes); $i++) { 
            $arr = $Clientes[$i];
            fputcsv($fp,$arr);
        }
        fclose($fp);
        return "OK";
    } else {
        return "El usuario ya existe.";
    }
    
}
/**
 * Borrar cliente a partir de su id
 *
 * @param [string] $id
 * @return void
 */
function ClienteDelete($id){
    $fp = fopen("clientes.csv", "r");
    $Clientes = [];
    $linea_Cliente = [];
    $i = 0;
    do {
        $linea_Cliente = fgetcsv($fp);
        if ($linea_Cliente) $Clientes[$i] = $linea_Cliente;
        $i++;
    }  while (!feof($fp));
    fclose($fp);
    for ($i=0; $i < sizeof($Clientes); $i++) { 
        if ($Clientes[$i][0] == $id){
            unset($Clientes[$i]);
            break;
        }
    }
    $fp = fopen("clientes.csv", "w");
    foreach ($Clientes as $value) {
        fputcsv($fp,$value);
    }
    fclose($fp);
}


/*
PEDIDOS
*/
/**
 * Comprobar el id de todos los pedidos
 *
 * @return [array] $ind Nos devueve un array con todos los índices.
 */
function comprobar_id_Pedido () {
    $clie = PedidoReadAll();
    $ind = array();
    foreach ($clie as $value) {
        array_push($ind,$value->getId_pedido());
    }
    return $ind;
}
/**
 * Leemos todos los pedidos
 *
 * @return [array] $Pedidos Array de objetos con todos los pedidos.
 */
function PedidoReadAll() {
    $fp = fopen("pedidos.csv", "r");
    $Pedidos = [];
    $linea_Pedido = [];
    $i = 0;
    while (!feof($fp)){
        $linea_Pedido = fgetcsv($fp);
        $Pedidos[$i] = new Pedido ($linea_Pedido[0],$linea_Pedido[1],$linea_Pedido[2]);
        $i++;
    }
    unset($Pedidos[$i-1]);
    return $Pedidos;
    fclose($fp); 
}
/**
 * Crear pedido a partir de un objeto Pedido
 *
 * @param [object] $Pedido
 * @return [string] Errores generados
 */
function PedidoCreate($Pedido) {
    $ind = comprobar_id_Pedido ();
    $fp = fopen("pedidos.csv", "a");
    
    if (in_array($Pedido -> getId_pedido(), $ind, true)) {
        echo "Se encontró indice repetido. El próximo id libre en el contador es ".(max($ind)+1).".\n";
    } else {
        $arr = array($Pedido -> getId_pedido(), $Pedido -> getId_cliente(), $Pedido -> getFecha());
        fputcsv($fp,$arr);
    }    
    fclose($fp);
}
/**
 * Leemos un pedido a partir del id
 *
 * @param [string] $id
 * @return [string] Errores de la función
 */
function PedidoRead($id){
    $Pedidos = PedidoReadAll(); 
    $fp = fopen("pedidos.csv", "a");
    foreach ($Pedidos as $value) {
        if ($value -> getId_pedido() == $id){
            return $value;
            break;
        }
    }
    return "No existe";
    fclose($fp);
}
/**
 * Actualizamos el listado de Pedidos
 *
 * @param [object] $Pedido
 * @return void
 */
function PedidoUpdate($Pedido){
    $fp = fopen("pedidos.csv", "r");
    $Pedidos = [];
    $linea_Pedido = [];
    $i = 0;
    do {
        $linea_Pedido = fgetcsv($fp);
        if ($linea_Pedido) $Pedidos[$i] = $linea_Pedido;
        $i++;
    }  while (!feof($fp));

    fclose($fp);
    for ($i=0; $i < sizeof($Pedidos); $i++) { 
        if ($Pedidos[$i][0] == $Pedido -> getId_pedido()){
            $Pedidos[$i] = array($Pedido -> getId_pedido(), $Pedido -> getId_cliente(), $Pedido -> getFecha());

            break;
        }
    }

    $fp = fopen("pedidos.csv", "w");
    for ($i=0; $i < sizeof($Pedidos); $i++) { 
        $arr = $Pedidos[$i];
        fputcsv($fp,$arr);
    }
    fclose($fp);
}
/**
 * Borramos Pedido a partir del id
 *
 * @param [type] $id
 * @return [string] Errores de la función
 */
function PedidoDelete($id){
    $fp = fopen("pedidos.csv", "r");
    $Pedidos = [];
    $linea_Pedido = [];
    $i = 0;
    do {
        $linea_Pedido = fgetcsv($fp);
        if ($linea_Pedido) $Pedidos[$i] = $linea_Pedido;
        $i++;
    }  while (!feof($fp));
    fclose($fp);
    for ($i=0; $i < sizeof($Pedidos); $i++) { 
        if ($Pedidos[$i][0] == $id){
            unset($Pedidos[$i]);
            break;
        }
    }
    $fp = fopen("pedidos.csv", "w");
    foreach ($Pedidos as $value) {
        fputcsv($fp,$value);
    }
    fclose($fp);
}

/*
LINEAS
*/
/**
 * Comprobamos los num de lineas en funcion del pedido
 *
 * @param [string] $ped Id del pedido
 * @return [array] $ind listado de todos los numeros de linea
 */
function comprobar_id_Lineas ($ped) {
    $lin = LineasReadAll();
    $ind = [];
    foreach ($lin as $value) {
        if ($value -> getId_pedido() == $ped){
            array_push($ind,$value->getNum_linea());
        }
    }
    return $ind;
}
/**
 * Leemos todas las lineas del archivo de lineas
 *
 * @return [array] $Lineas array de objetos lineas
 */
function LineasReadAll() {
    $fp = fopen("lineas.csv", "r");
    $Lineas = [];
    $linea_Lineas = [];
    $i = 0;
    while (!feof($fp)){
        $linea_Lineas = fgetcsv($fp);
        $Lineas[$i] = new lineas ($linea_Lineas[0], $linea_Lineas[1], $linea_Lineas[2],$linea_Lineas[3],$linea_Lineas[4]);
        $i++;
    }
    unset($Lineas[$i-1]);
    return $Lineas;
    fclose($fp); 
}
/**
 * Creamos Lineas a partir de un objeto lineas
 *
 * @param [object] $Lineas Objeto a añadir
 * @return void
 */
function LineasCreate($Lineas) {
    $fp = fopen("lineas.csv", "a");
    $arr = array($Lineas -> getNum_linea(), $Lineas -> getId_articulo(), $Lineas -> getNombre(), $Lineas -> getPrecio(), $Lineas -> getId_pedido());
    fputcsv($fp,$arr);  
    fclose($fp);
}
/**
 * Devuelve array de objetos lineas con la del pedido.
 *
 * @param [srring] $id_pedido Id del pedido del que queremos conocer las lineas
 * @return [srring] $todas_las_lineas_pedido Todas las lineas del pedido.
 */
function LineasRead($id_pedido){
    $Lineass = LineasReadAll();
    $todas_las_lineas_pedido = [];
    foreach ($Lineass as $value) {
        if ($value -> getId_pedido() == $id_pedido){
            array_push($todas_las_lineas_pedido, $value);
        }
    }
    if($todas_las_lineas_pedido) {
        return $todas_las_lineas_pedido;
    } else {
        return "Sin datos";
    }
}
/**
 * Actualizamos las Lineas
 *
 * @param [object] $Lineas
 * @return [string] Errores
 */
function LineasUpdate($Lineas){
    $fp = fopen("lineas.csv", "r");
    $Lineass = [];
    $linea_Lineas = [];
    $i = 0;
    do {
        $linea_Lineas = fgetcsv($fp);
        if ($linea_Lineas) $Lineass[$i] = $linea_Lineas;
        $i++;
    }  while (!feof($fp));

    fclose($fp);
    //Hay una doble clave en este archivo
    for ($i=0; $i < sizeof($Lineass); $i++) { 
        if ($Lineass[$i][4] == $Lineas -> getId_pedido()){
            if ($Lineass[$i][0] == $Lineas -> getNum_linea()){
                $Lineass[$i] = array($Lineas -> getNum_linea(), $Lineas -> getId_articulo(), $Lineas -> getNombre(), $Lineas -> getPrecio(), $Lineas -> getId_pedido());
                break;
            }
        }
    }

    $fp = fopen("lineas.csv", "w");
    for ($i=0; $i < sizeof($Lineass); $i++) { 
        $arr = $Lineass[$i];
        fputcsv($fp,$arr);
    }
    fclose($fp);
}
/**
 * Borramos Líneas, necesitamos el numero de linea y el pedido al cual pertenece
 *
 * @param [string] $id_pedido
 * @param [string] $num_linea
 * @return void
 */
function LineasDelete($id_pedido,$num_linea){
    $fp = fopen("lineas.csv", "r");
    $Lineass = [];
    $linea_Lineas = [];
    $i = 0;
    do {
        $linea_Lineas = fgetcsv($fp);
        if ($linea_Lineas) $Lineass[$i] = $linea_Lineas;
        $i++;
    }  while (!feof($fp));
    fclose($fp);
    for ($i=0; $i < sizeof($Lineass); $i++) { 
        if ($Lineass[$i][4] == $id_pedido && $Lineass[$i][0]==$num_linea){
            unset($Lineass[$i]);
            break;
        }
    }
    $fp = fopen("lineas.csv", "w");
    foreach ($Lineass as $value) {
        fputcsv($fp,$value);
    }
    fclose($fp);
}

/* LOGIN */
/**
 * Comprobamos el usuario y contraseña de la página login
 *
 * @param [string] $usuario
 * @param [string] $password
 * @return [string] Mensajes de error u OK
 */
function login_usuario ($usuario,$password){
    $Clientes = ClienteReadAll();
	foreach ($Clientes as $value) {
		if ($value -> getUsuario() == $usuario){
			if ($value -> getPassword() == $password) {
                $fp = fopen("usr", "w");
                fwrite($fp, $value -> getId());
                fclose($fp);
                return "OK";
                break;
			} else {
                return "Error en la contraseña.";
                break;
            }
        }      
	}
    return "El usuario ". $usuario ." no existe";
}
/**
 * Leemos el usuario que está logado actualmente
 *
 * @return void
 */
function leer_usr () {
    $fp = fopen("usr", "r");
    $usr = fgetss($fp);
    fclose($fp);
    return $usr;
}
