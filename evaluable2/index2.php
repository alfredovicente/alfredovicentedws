<?php
/**
 * @file index2.php
* @brief Archivo de índice de la página
*
* Menu con todos los enlaces para acceder.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/
/**
 * @mainpage Evaluable 2 
 * <h1>Apartados de la evaluable</h1> 
 *
1.Realizar el crud de cada fichero punto por cada fichero con las operaciones siguientes. ( 1punto por fichero, total 4 puntos) i.Create 0,5 puntos
2.Read 0,5 puntos
3.Update 0,5 puntosi
4.Delete 0,5 puntos

Todos los Formularios hacen peticiones a un archivo denominado control.php el cual hace las peticiones a los ficheros a través del archivo fichero.php

<h1>fichero.php</h1>

2.Se crean las clases para recoge los datos en objetos leidos en el formulario y operaciones del crud en fichero.php. 
Tendrá 1 punto por fichero. 
Total 4 puntos 

Dentro de la carpeta classes están definidas todas las clases de todos de cada uno de objetos que vamos a necesitar.
 
<h1>Evaluable 1</h1>
3.La aplicación cumple con lo solicitado en la evaluable1 con formularios revisados. 1 punto

En la evaluable 1 se pedía:

1. Formulario 5

Los formularios cumplen con lo que se pedía en la evaluable 1. Se han añadido el uso de imágenes que se pidió por teléfono.

2. Aspecto 1

El aspecto es el pedido y mostrado en imágenes en ambas evaluables.

3. Login 1

Hace login sobre, el id del usuario logado se guarda en un fichero llamado usr.

4. Uso de funciones 1

Se hace uso de funciones en todo momento.

5. Includes y- Configuración 1

Se hace uso de includes y de require en todo momento.

6. Git/Heroku 1

Se ha iniciado el repositorio y se ha subido perfectamente.

Recuerdo que, tal y como comentamos en conversación telefónica. Una vez revisado en esta evaluable 2, todos los puntos me pondrias un 5 en la evaluable1.

4.Presentación, Documentación, Heroku, Bitbucket. 1 punto

Se uso de los repositorios, y se añade esta documentación.

<h1>Objetos</h1>

La idea es que la aplicación pase a utilizar objetos como elemento básico de información.

El elemento de información es en todos momento los objetos. 

* <p/> <br/>
 */


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>

<body>
    <h1>Evaluable 2</h1>
    <a href="../index.php">Volver</a>
    <br>
    <div>
        <h2>Menu:</h2>
        <ul>
            <li><a href="articulos.php">Artículos</a></li>
            <li><a href="clientes.php">Clientes</a></li>
            <li><a href="pedidos.php">Hacer Pedidos</a></li>
            <li><a href="listado_pedidos.php">Listado Pedidos</a></li>
            <br>
            <li><a href="DWST05Evaluable2.pdf">Enunciado</a></li>
            <li><a href="html/index.html">Documento alumno</a></li>
        </ul>
    </div>
    <hr>
    <a>2019-20 DWS Evaluable2 Alfredo Vicente Boix</a>
</body>

</html>
