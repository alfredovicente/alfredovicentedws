<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>


<body>
    <h1>Actualizar Pedidos</h1>
    <h2>Menu</h2>
    <div>
    		
            <a href="listado_pedidos.php">Volver</a>
            <br/><br/>
<?php
/**
* @file actualizar_lineas.php
* @brief Archivo para actualizar Lineas de un pedido
*
* Formulario que actualiza lineas de un pedido
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/

/**
 * Parámetro recibido por método GET
 */
$id=$_GET['idPedido'];

require('fichero.php');
$result2 = LineasRead($id);

echo "<h2>Listado del pedido</h2>";
echo "<table border='1'>";
echo "<tr><td>num_linea</td><td>id_articulo</td><td>nombre</td><td>precio</td><td>id_pedido</td></tr>";

foreach ($result2 as $value) {
	$j = $value->getNum_linea();
	echo '<form action="control.php" method="post" enctype="multipart/form-data">';

			echo "<tr>"; 
			echo "<td><input type='number' name='num_linea' value='".$value->getNum_linea()."' readonly/></td>";
			echo "<td><input type='number' name='id_articulo' value='".$value->getId_articulo()."'/></td>";
			echo "<td><input type='text' name='nombre' value='".$value->getNombre()."'/></td>";
			echo "<td><input type='number' name='precio' value='".$value->getPrecio()."'/></td>";
			echo "<td><input type='number' name='id_pedido' value='".$value->getId_pedido()."' readonly/></td>";
			echo "<td><a href='control.php?action=borrar_linea&idPedido=".$value->getId_pedido()."&num_Linea=".$value->getNum_linea()."'>Borrar</a></td>";			
			echo '<input type="text" name="action" value="actualizar_linea" hidden>';

			echo '<td><input type="submit" value="Actualizar"></td>';

			echo "</tr>";
			echo "</form>";

	}

echo "</table>";
?>

	</div>
	




    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>