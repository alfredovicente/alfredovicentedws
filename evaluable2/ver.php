<?php
/**
 * @file ver.php
* @brief Archivo para ver todos los artículos
*
* Se crea una tabla para ver en detalle cada uno de los artículos
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>

<body>
    <h1>Artículos</h1>
    <h2>Menu</h2>
    <div>

            <a href="articulos.php">Volver</a><br><br>

<?php
require('fichero.php');
$val = $_GET['id'];
$result = ArticuloRead($val);
echo "<table border='1'>";
echo "<tr><td>key</td><td>valor</td></tr>";

    echo "<tr>"; 
    echo "<td>Id Articulo</td><td>".$result->getId()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>Nombre</td><td>".$result->getNombre()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>Precio</td><td>".$result->getPrecio()."</td>";
    echo "</tr>";

echo "</table>";



?>


    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
