<?php
/**
 * @file nuevo.php
* @brief Archivo de Nuevo artículo
*
* Formulario para rellenar nuevos artículos
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 31/12/2019
*/
session_start();
setcookie(session_name(),session_id(),time()+600);
if(!$_SESSION["idCliente"]){
        header("Location: login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>


<?php
if(isset($_GET['idArticulo'])){
    $id=$_GET['idArticulo'];
} else {
    if($_SESSION['datos']=='csv'){
    require_once("fichero.php");
    } elseif ($_SESSION['datos']=='bd') {
    require_once("postgres.php");
    }
    $ind = comprobar_id_art();
    $id = max($ind)+1;

}
if(isset($_GET['nombre'])){
    $nombre=$_GET['nombre'];
} else {
    $nombre='';
}
if(isset($_GET['precio'])){
    $precio=$_GET['precio'];
} else {
    $precio='';
}

?>
<body>
    <h1>Nuevo Artículo</h1>
    <h2>Menu</h2>
    <div>
    		
            <a href="articulos.php">Volver</a>
            <br/><br/>
            <form action="control.php" method="post" enctype="multipart/form-data">
            	<table border='1'>
            		<tr>
                        <td>Id</td>
                        <td><input type="number" name="idArticulo" value="<?php echo $id ?>" readonly></td>
                    </tr>
                    <tr>
                        <td>Nombre</td>
                        <td><input type="text" name="nombre" value="<?php echo $nombre ?>"></td>
                    </tr>
                    <tr>
                        <td>Precio</td>
                        <td><input type="number" name="precio" step="0.1" value="<?php echo $precio ?>"></td>
                    </tr>
                    <tr>
                        <td>Imagen</td>
                        <td><input type="file" name="imagen"></td>
                    </tr>
            		<input type="text" name="action" value="nuevo" hidden>

            	</table>
            	<br>
            	<input type="submit" value="Inserta">
	            <input type="button" value="Borrar" onclick="javascript:location.href='nuevo.php?idArticulo=&nombre&precio=;'">
            </form>
            <br>
            <div style='color: red'>
    			<?php
    			if(isset($_GET['anadido'])){
					echo "El valor ha sido anadido";
				} else if (isset($_GET['error'])) {
                    echo "Ha habido un error ".$_GET['error'];
                }
				?>
    		</div>




    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>