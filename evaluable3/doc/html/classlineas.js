var classlineas =
[
    [ "__construct", "classlineas.html#a93b8420d822651ddee0455713b5f382b", null ],
    [ "getId_articulo", "classlineas.html#a56cb7e817a3b390bae690ec19c16b89d", null ],
    [ "getId_pedido", "classlineas.html#a357c185dab809a9af2701f5cb22a90e3", null ],
    [ "getNombre", "classlineas.html#a19ecce3d19e108b3bdfb5c8e74512062", null ],
    [ "getNum_linea", "classlineas.html#a465ce1ae629d9bfecc65f799187ca417", null ],
    [ "getPrecio", "classlineas.html#ae1f5d7a6ecf229ebc209b561477ae5ec", null ],
    [ "setId_articulo", "classlineas.html#aebd6dd180dbc97f1a5eba3dccfc7fffc", null ],
    [ "setId_pedido", "classlineas.html#a75902c9376a36ac213104362397c1307", null ],
    [ "setNombre", "classlineas.html#a9bf28877fffefe5d4d7aae9f9989fc79", null ],
    [ "setNum_linea", "classlineas.html#a0b685e1a4a6f9537da347f89c080a4c5", null ],
    [ "setPrecio", "classlineas.html#a8b93afd26d560c4b070b96ad515f4d22", null ]
];