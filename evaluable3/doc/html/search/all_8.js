var searchData=
[
  ['leer_5fusr',['leer_usr',['../fichero_8php.html#a81a1266d1488b1c709a0c075fc34b3a3',1,'leer_usr():&#160;fichero.php'],['../postgres_8php.html#a81a1266d1488b1c709a0c075fc34b3a3',1,'leer_usr():&#160;postgres.php']]],
  ['linea_2ephp',['linea.php',['../linea_8php.html',1,'']]],
  ['lineas',['lineas',['../classlineas.html',1,'']]],
  ['lineascreate',['LineasCreate',['../fichero_8php.html#a05fe4ad6bbc7127cbfa60643ee68b7ab',1,'LineasCreate($Lineas):&#160;fichero.php'],['../postgres_8php.html#a05fe4ad6bbc7127cbfa60643ee68b7ab',1,'LineasCreate($Lineas):&#160;postgres.php']]],
  ['lineasdelete',['LineasDelete',['../fichero_8php.html#a976add046601bf971f2d01f78b78408c',1,'LineasDelete($id_pedido, $num_linea):&#160;fichero.php'],['../postgres_8php.html#a976add046601bf971f2d01f78b78408c',1,'LineasDelete($id_pedido, $num_linea):&#160;postgres.php']]],
  ['lineasread',['LineasRead',['../fichero_8php.html#a1cbd47e2d964e3f75917134b79b705c4',1,'LineasRead($id_pedido):&#160;fichero.php'],['../postgres_8php.html#a1cbd47e2d964e3f75917134b79b705c4',1,'LineasRead($id_pedido):&#160;postgres.php']]],
  ['lineasreadall',['LineasReadAll',['../fichero_8php.html#a8123d6636f751144004d4c5157067261',1,'LineasReadAll():&#160;fichero.php'],['../postgres_8php.html#a8123d6636f751144004d4c5157067261',1,'LineasReadAll():&#160;postgres.php']]],
  ['lineasupdate',['LineasUpdate',['../fichero_8php.html#a24fdb9eab36b6a821835e7f10d0d7bdd',1,'LineasUpdate($Lineas):&#160;fichero.php'],['../postgres_8php.html#a24fdb9eab36b6a821835e7f10d0d7bdd',1,'LineasUpdate($Lineas):&#160;postgres.php']]],
  ['listado_5fpedidos_2ephp',['listado_pedidos.php',['../listado__pedidos_8php.html',1,'']]],
  ['login_5fusuario',['login_usuario',['../fichero_8php.html#a65d29fd74df7b79ca2b5933020ea5021',1,'login_usuario($usuario, $password):&#160;fichero.php'],['../postgres_8php.html#a65d29fd74df7b79ca2b5933020ea5021',1,'login_usuario($usuario, $password):&#160;postgres.php']]]
];
