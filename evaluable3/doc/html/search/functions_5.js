var searchData=
[
  ['setemail',['setEmail',['../class_cliente.html#a5ef76eef42d2624386442eeb636d338c',1,'Cliente']]],
  ['setfecha',['setFecha',['../class_cliente.html#af725b12ac487c1a2a26b311408cdfa2b',1,'Cliente\setFecha()'],['../class_pedido.html#af725b12ac487c1a2a26b311408cdfa2b',1,'Pedido\setFecha()']]],
  ['setid',['setId',['../class_cliente.html#a87313ad678fb2a2a8efb435cf0bdb9a0',1,'Cliente']]],
  ['setid_5farticulo',['setId_articulo',['../classlineas.html#aebd6dd180dbc97f1a5eba3dccfc7fffc',1,'lineas']]],
  ['setid_5fcliente',['setId_cliente',['../class_pedido.html#a5b6823c1326d493d5c29242d06832a78',1,'Pedido']]],
  ['setid_5fpedido',['setId_pedido',['../classlineas.html#a75902c9376a36ac213104362397c1307',1,'lineas\setId_pedido()'],['../class_pedido.html#a75902c9376a36ac213104362397c1307',1,'Pedido\setId_pedido()']]],
  ['setnombre',['setNombre',['../class_cliente.html#a9bf28877fffefe5d4d7aae9f9989fc79',1,'Cliente\setNombre()'],['../classlineas.html#a9bf28877fffefe5d4d7aae9f9989fc79',1,'lineas\setNombre()']]],
  ['setnum_5flinea',['setNum_linea',['../classlineas.html#a0b685e1a4a6f9537da347f89c080a4c5',1,'lineas']]],
  ['setpassword',['setPassword',['../class_cliente.html#a3e35c8d3dbb2c513c618a664389e0926',1,'Cliente']]],
  ['setprecio',['setPrecio',['../classlineas.html#a8b93afd26d560c4b070b96ad515f4d22',1,'lineas']]],
  ['setusuario',['setUsuario',['../class_cliente.html#a48255b4550c8d4dc7aedf7837439461a',1,'Cliente']]]
];
