var searchData=
[
  ['actualizar_2ephp',['actualizar.php',['../actualizar_8php.html',1,'']]],
  ['actualizar_5fclientes_2ephp',['actualizar_clientes.php',['../actualizar__clientes_8php.html',1,'']]],
  ['actualizar_5flineas_2ephp',['actualizar_lineas.php',['../actualizar__lineas_8php.html',1,'']]],
  ['actualizar_5fpedido_2ephp',['actualizar_pedido.php',['../actualizar__pedido_8php.html',1,'']]],
  ['articulo_2ephp',['articulo.php',['../articulo_8php.html',1,'']]],
  ['articulocreate',['ArticuloCreate',['../fichero_8php.html#a001ee8896aaf475b2a8050a6139c9fe1',1,'ArticuloCreate($Articulo):&#160;fichero.php'],['../postgres_8php.html#a984e433ff912e17f8f68a46f778da91d',1,'ArticuloCreate($Art):&#160;postgres.php']]],
  ['articulodelete',['ArticuloDelete',['../fichero_8php.html#a0c9753f2760a4ce1f73f7b30537a44a8',1,'ArticuloDelete($id):&#160;fichero.php'],['../postgres_8php.html#a0c9753f2760a4ce1f73f7b30537a44a8',1,'ArticuloDelete($id):&#160;postgres.php']]],
  ['articuloread',['ArticuloRead',['../fichero_8php.html#a57ad4a4983c7089b605182513585f4b0',1,'ArticuloRead($id):&#160;fichero.php'],['../postgres_8php.html#a57ad4a4983c7089b605182513585f4b0',1,'ArticuloRead($id):&#160;postgres.php']]],
  ['articuloreadall',['ArticuloReadAll',['../fichero_8php.html#a19c628a5a084c225bbb1cf71a9c4d07f',1,'ArticuloReadAll():&#160;fichero.php'],['../postgres_8php.html#a19c628a5a084c225bbb1cf71a9c4d07f',1,'ArticuloReadAll():&#160;postgres.php']]],
  ['articulos',['articulos',['../classarticulos.html',1,'']]],
  ['articulos_2ephp',['articulos.php',['../articulos_8php.html',1,'']]],
  ['articuloupdate',['ArticuloUpdate',['../fichero_8php.html#a267f3585f2424021e23a7d769f5ae798',1,'ArticuloUpdate($Articulo):&#160;fichero.php'],['../postgres_8php.html#a005294db2cff415fdce5428ca554b29a',1,'ArticuloUpdate($Art):&#160;postgres.php']]]
];
