var class_cliente =
[
    [ "__construct", "class_cliente.html#ab9e2aa975d3e1389a43124a7d80ba3b8", null ],
    [ "getEmail", "class_cliente.html#a02a01849f28e2535e888ae4ec87b20f2", null ],
    [ "getFecha", "class_cliente.html#a50078690f0773491fd4a7cbbca4e280f", null ],
    [ "getId", "class_cliente.html#a12251d0c022e9e21c137a105ff683f13", null ],
    [ "getNombre", "class_cliente.html#a19ecce3d19e108b3bdfb5c8e74512062", null ],
    [ "getPassword", "class_cliente.html#a04e0957baeb7acde9c0c86556da2d43f", null ],
    [ "getUsuario", "class_cliente.html#a38abe751ca21ffb95d9c90862e0fb96f", null ],
    [ "setEmail", "class_cliente.html#a5ef76eef42d2624386442eeb636d338c", null ],
    [ "setFecha", "class_cliente.html#af725b12ac487c1a2a26b311408cdfa2b", null ],
    [ "setId", "class_cliente.html#a87313ad678fb2a2a8efb435cf0bdb9a0", null ],
    [ "setNombre", "class_cliente.html#a9bf28877fffefe5d4d7aae9f9989fc79", null ],
    [ "setPassword", "class_cliente.html#a3e35c8d3dbb2c513c618a664389e0926", null ],
    [ "setUsuario", "class_cliente.html#a48255b4550c8d4dc7aedf7837439461a", null ],
    [ "$password", "class_cliente.html#a607686ef9f99ea7c42f4f3dd3dbb2b0d", null ]
];