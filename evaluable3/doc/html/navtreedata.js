var NAVTREE =
[
  [ "Evaluable 3", "index.html", [
    [ "intro", "md_intro.html", null ],
    [ "Estructuras de Datos", "annotated.html", [
      [ "Estructura de datos", "annotated.html", "annotated_dup" ],
      [ "Índice de estructura de datos", "classes.html", null ],
      [ "Campos de datos", "functions.html", [
        [ "Todo", "functions.html", null ],
        [ "Funciones", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Archivos", null, [
      [ "Lista de archivos", "files.html", "files" ],
      [ "Globales", "globals.html", [
        [ "Todo", "globals.html", null ],
        [ "Funciones", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"actualizar_8php.html"
];

var SYNCONMSG = 'click en deshabilitar sincronización';
var SYNCOFFMSG = 'click en habilitar sincronización';