var fichero_8php =
[
    [ "ArticuloCreate", "fichero_8php.html#a001ee8896aaf475b2a8050a6139c9fe1", null ],
    [ "ArticuloDelete", "fichero_8php.html#a0c9753f2760a4ce1f73f7b30537a44a8", null ],
    [ "ArticuloRead", "fichero_8php.html#a57ad4a4983c7089b605182513585f4b0", null ],
    [ "ArticuloReadAll", "fichero_8php.html#a19c628a5a084c225bbb1cf71a9c4d07f", null ],
    [ "ArticuloUpdate", "fichero_8php.html#a267f3585f2424021e23a7d769f5ae798", null ],
    [ "ClienteCreate", "fichero_8php.html#aae1e39d936535d23ad502704bef74d64", null ],
    [ "ClienteDelete", "fichero_8php.html#a9d076506c7bd7f806c021219f36ee594", null ],
    [ "ClienteRead", "fichero_8php.html#a40661ee6e696ba8c32f16cb02686427d", null ],
    [ "ClienteReadAll", "fichero_8php.html#a512ee46330f0c71f734d5825742b94c6", null ],
    [ "ClienteReadUser", "fichero_8php.html#a6f82c5dc580157cd02697c2b6a01c64f", null ],
    [ "ClienteUpdate", "fichero_8php.html#a7905e6dbf777fbc5f913d75624ec6533", null ],
    [ "comprobar_id_art", "fichero_8php.html#a1658ebffac7bdbcf78813b24d103d270", null ],
    [ "comprobar_id_cliente", "fichero_8php.html#af64c728a9599c908b24aa63cb23c8518", null ],
    [ "comprobar_id_Lineas", "fichero_8php.html#aa53f50d4a0dfab7033df28b0557f2e52", null ],
    [ "comprobar_id_Pedido", "fichero_8php.html#a1b91b25c7e3fa0b9b14324253abb2662", null ],
    [ "comprobar_usuario", "fichero_8php.html#a91d5fbc8aaaf89c5ea5b7d2f2a5a967d", null ],
    [ "leer_usr", "fichero_8php.html#a81a1266d1488b1c709a0c075fc34b3a3", null ],
    [ "LineasCreate", "fichero_8php.html#a05fe4ad6bbc7127cbfa60643ee68b7ab", null ],
    [ "LineasDelete", "fichero_8php.html#a976add046601bf971f2d01f78b78408c", null ],
    [ "LineasRead", "fichero_8php.html#a1cbd47e2d964e3f75917134b79b705c4", null ],
    [ "LineasReadAll", "fichero_8php.html#a8123d6636f751144004d4c5157067261", null ],
    [ "LineasUpdate", "fichero_8php.html#a24fdb9eab36b6a821835e7f10d0d7bdd", null ],
    [ "login_usuario", "fichero_8php.html#a65d29fd74df7b79ca2b5933020ea5021", null ],
    [ "PedidoCreate", "fichero_8php.html#a3db408a96b9c9672507beb19a69f651c", null ],
    [ "PedidoDelete", "fichero_8php.html#a6abd8a89969c7158f77d6f38a07c1dc4", null ],
    [ "PedidoRead", "fichero_8php.html#a2e16e0db19f23e203664d904a25a98f3", null ],
    [ "PedidoReadAll", "fichero_8php.html#a6f986569a8c10d0de989ce3f0846b7b1", null ],
    [ "PedidoUpdate", "fichero_8php.html#abe6678e1b5a076fb55d7e6f39c356f88", null ]
];