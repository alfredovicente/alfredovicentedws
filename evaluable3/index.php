<?php
/**
 * @file index.php
* @brief Archivo de índice de la página
*
* Menu con todos los enlaces para acceder.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/
/**
 * @mainpage Evaluable 3 
 * <h1>Apartados de la evaluable</h1> 
 *
 
<h1>Datos</h1>

Alumno:Alfredo Rafael Vicente Boix
 
<h1>Evaluable 3</h1>
 
 1. Base de datos, con eliminación de la inyección de sql a través de consultas preparadas. 
 
SE han usado SQL preparadas que se envían aela base de datos de forma separada a cualquier parámetro. De esta forma es imposible para un atacante inyectar SQL malicioso. Es la forma más recomendable y segura de evitar este tipo de ataques.
 
2. Sesiones y Cookies.

Se ha usado control de sesiones y cookies para delimitar el tiempo de inactividad en una pagina. Las variables de sesion nos han permitido alternar entre acceso a ficheros y acceso a base de datos sin tocar el codigo.

3. Elección del sistema de almacenamiento. Uso de sesiones para almacenar sistema de almacenamiento. Se utilizará un interface llamado Idatos.php con las cabeceras de las funciones de acceso a la fuente de datos.

En lugar de idatos hemos utilizado control.php que se encarga de gestionar las peticiones. Se carga el fichero dependiendo de si hemos escogido base de datos o csv.

4. Presentación, Estilos, Documentación.

Se ha usado doygen para documentar. OJO! Todos los archivos estan documentados. Doxygen se usa para facilitar la lectura de los archivos. Se han usado los mismos formatos de ejemplo.

5. Heroku - Postgres, Bitbucket.

Se han usado los repositorios y se ha hecho el despleigue en heroku. 
 
<h1>Evaluable 2</h1>

1.Realizar el crud de cada fichero punto por cada fichero con las operaciones siguientes. ( 1punto por fichero, total 4 puntos) i.Create 0,5 puntos
2.Read 0,5 puntos
3.Update 0,5 puntosi
4.Delete 0,5 puntos

Todos los Formularios hacen peticiones a un archivo denominado control.php el cual hace las peticiones a los ficheros a través del archivo fichero.php

<h3>fichero.php</h3>

2.Se crean las clases para recoge los datos en objetos leidos en el formulario y operaciones del crud en fichero.php. 
Tendrá 1 punto por fichero. 
Total 4 puntos 

Dentro de la carpeta classes están definidas todas las clases de todos de cada uno de objetos que vamos a necesitar.
 
<h1>Evaluable 1</h1>
3.La aplicación cumple con lo solicitado en la evaluable1 con formularios revisados. 1 punto

En la evaluable 1 se pedía:

1. Formulario 5

Los formularios cumplen con lo que se pedía en la evaluable 1. Se han añadido el uso de imágenes que se pidió por teléfono.

2. Aspecto 1

El aspecto es el pedido y mostrado en imágenes en ambas evaluables.

3. Login 1

Hace login sobre, el id del usuario logado se guarda en un fichero llamado usr.

4. Uso de funciones 1

Se hace uso de funciones en todo momento.

5. Includes y- Configuración 1

Se hace uso de includes y de require en todo momento.

6. Git/Heroku 1

Se ha iniciado el repositorio y se ha subido perfectamente.

Recuerdo que, tal y como comentamos en conversación telefónica. Una vez revisado en esta evaluable 2, todos los puntos me pondrias un 5 en la evaluable1.

4.Presentación, Documentación, Heroku, Bitbucket. 1 punto

Se uso de los repositorios, y se añade esta documentación.

<h1>Objetos</h1>

La idea es que la aplicación pase a utilizar objetos como elemento básico de información.

El elemento de información es en todos momento los objetos. 

* <p/> <br/>
 */
session_start();
setcookie(session_name(),session_id(),time()+600);

if(!$_SESSION["idCliente"]){
        header("Location: login.php");
}
//var_dump($_SESSION);
//$session_name = session_name();
//var_dump($_COOKIE);
//var_dump(session_get_cookie_params ());
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>

<body>
    <h1>Evaluable 3</h1>
    <a href="../index.php">Volver</a>
    <br>
    <div>
        <h2>Menu:</h2>
        <ul>
            <li><a href="articulos.php">Artículos</a></li>
            <li><a href="clientes.php">Clientes</a></li>
            <li><a href="pedidos.php">Hacer Pedidos</a></li>
            <li><a href="listado_pedidos.php">Listado Pedidos</a></li>
            <br>
            <li><a href="control.php?action=salir">Cerrar Sesion</a></li>
            <br>
            <li><a href="DWST07Evaluable3.pdf">Enunciado</a></li>
            <li><a href="./doc/html/index.html">Documentación</a></li>
        </ul>
    </div>
    <hr>
    <a>2019-20 DWS Evaluable3 Alfredo Vicente Boix</a>
</body>

</html>
