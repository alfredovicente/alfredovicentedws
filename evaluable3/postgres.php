
<?php
/**
 * @file postgres.php
* @brief Archivo que gestiona el acceso a todos los ficheros
*
* Funciones que acceden a la base de datos y sacan los valores y escribe en ellos.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 31/12/2019
*/
require('bd.php');
require("classes/articulo.php");
require("classes/cliente.php");
require("classes/linea.php");
require("classes/pedido.php");


/**
 * Esta función nos permite comprobar todos los id de un 
 * archivo para que no se repitan.
 *
 * @return [array] $ind Devuelve todos los índices que hay.
 */
function comprobar_id_art () {
    $arts = ArticuloReadAll();
    $ind = array();
    foreach ($arts as $value) {
        array_push($ind,$value->getId());
    }
    return $ind;
}
/**
 * Lee todos los artículos que hay en el archivo de artículos.
 *
 * @return array 
 */
function ArticuloReadAll() {
    //$fp = fopen("articulos.csv", "r");
    
    $cons = consulta("SELECT * FROM articulo",false);
    
    $articulos = [];
    $linea = "";
    $linea_articulo = [];
    $i = 0;
    foreach ($cons as $valor) {
        //var_dump($valor);
        $linea_articulo = $valor;
        //$linea_articulo = explode(";",$linea);
        $articulos[$i] = new articulos ($linea_articulo['id'],$linea_articulo['nombre'],$linea_articulo['precio']);
        $i++;
    }
    return $articulos;
    //fclose($fp); 
}


/**
 * Crea un artículo a partir de un artículo dado
 *
 * @param [articulo] $Articulo Les pasamos un objeto
 * @return void
 */
function ArticuloCreate($Art) {
    $ind = comprobar_id_art ();
    //$fp = fopen("articulos.csv", "a");
   
    if (in_array($Art -> getId(), $ind, true)) {
        $error['error']= "Se encontró indice repetido. El próximo id libre en el contador es ".(max($ind)+1).".\n";
		header("Location: error.php?error=$error[error]");
    } else {
        $arr = array($Art -> getId(), $Art -> getNombre(), $Art -> getPrecio());
        //fputcsv($fp,$arr);
        $query = "INSERT INTO articulo (id,nombre,precio) VALUES (?,?,?);";
        introduce($query,$arr);
    }    
    //fclose($fp);
}

/**
 * Leemos un artículo dada una id
 *
 * @param [string] $id
 * @return [object] $value
 */
function ArticuloRead($id){
    $arr = array($id);
    $linea_articulo = consulta("SELECT * FROM articulo WHERE id=?",$arr);
    if ($linea_articulo) {
        $art = new articulos ($linea_articulo[0]['id'],$linea_articulo[0]['nombre'],$linea_articulo[0]['precio']);
        return $art;
    } else {
        return "No existe";
    }
}

/**
 * Actualizamos un articulo dado un objecto articulo
 *
 * @param [object] $Articulo
 * @return void
 */
function ArticuloUpdate($Art){
    //$fp = fopen("articulos.csv", "r");
    $ind = comprobar_id_art ();
    if (in_array($Art -> getId(), $ind, true)) {
         $arr = array($Art -> getId(), $Art -> getNombre(), $Art -> getPrecio());
        //fputcsv($fp,$arr);
        $query = "UPDATE articulo SET id ='$arr[0]', nombre='$arr[1]', precio='$arr[2]' WHERE  id='$arr[0]';";
        $ret = introduce($query);       
    } else {
        $error['error']= "No puedo actualizar un dato que no existe";
		header("Location: error.php?error=$error[error]");
    }
}

/**
 * Borrar articulo dado id
 *
 * @param [string] $id
 * @return void
 */
function ArticuloDelete($id){
    $arr=array($id);
    $query = "DELETE FROM articulo WHERE  id=?;";
    $ret = introduce($query,$arr);
}

/*
CLIENTES
*/

/**
 * Cogemos en un array todos los id de los clientes.
 *
 * @return [array] $ind
 */
function comprobar_id_cliente () {
    $clie = ClienteReadAll();
    $ind = [];
    foreach ($clie as $value) {
        array_push($ind,$value->getId());
    }
    return $ind;
}

/**
 * Con esta funcion leemos si el usuario está detro del archivo de usuarios
 *
 * @param [string] $usuario
 * @return [string]
 */
function comprobar_usuario ($usuario) {
    $clie = ClienteReadAll();
    $usr = [];
    foreach ($clie as $value) {
        array_push($usr,$value->getUsuario());
    }
    if (in_array($usuario, $usr)) {
        return "repe";
    } else {
        return "OK";
    }
    return $usr;
}

/**
 * Leemos todos los clientes
 *
 * @return [array] Todos los objetos clientes generados a partir del archivo de clientes.
 */
function ClienteReadAll() {
    $cons = consulta("SELECT * FROM cliente",false);
    $clientes = [];
    $linea_cliente = [];
    $i = 0;
    foreach ($cons as $valor) {
        //var_dump($valor);
        $linea_cliente = $valor;
        //$linea_articulo = explode(";",$linea);
        $clientes[$i] = new cliente ($linea_cliente['id'],$linea_cliente['nombre'],$linea_cliente['email'],$linea_cliente['fechanacimiento'],$linea_cliente['usuario'],$linea_cliente['password']);
        $i++;
    }
    return $clientes;
}

//var_dump(ClienteReadAll());
/**
 * Crear un cliente a partir de un objeto cliente dado.
 *
 * @param [object] $Cliente
 * @return [string] Con mensajes de error o OK
 */
function ClienteCreate($Cliente) {
    
    $ind = comprobar_id_cliente ();
    //$fp = fopen("articulos.csv", "a");
   
    if (in_array($Cliente -> getId(), $ind, true)) {
        $error['error']= "Se encontró indice repetido. El próximo id libre en el contador es ".(max($ind)+1).".\n";
		header("Location: error.php?error=$error[error]");
    } else {
        $arr = array($Cliente -> getId(), $Cliente -> getNombre(), $Cliente -> getEmail(), $Cliente -> getUsuario(), $Cliente -> getPassword(), $Cliente -> getFecha());
        //fputcsv($fp,$arr);
        var_dump($arr);
        $query = "INSERT INTO cliente(id,nombre,email,usuario,password,fechanacimiento) VALUES (?,?,?,?,?,?)";
        introduce($query,$arr);
        return "OK";
    }    
    //fclose($fp);
    
}

/**
 * Leemos un objeto cliente dado su id
 *
 * @param [string] $id
 * @return [string] Errores dados
 */
function ClienteRead($id){
    $arr=array($id);
    $Cliente = consulta("SELECT * FROM cliente WHERE id=?",$arr);
    $ret=new cliente ($Cliente[0]['id'], $Cliente[0]['nombre'], $Cliente[0]['email'], $Cliente[0]['fechanacimiento'], $Cliente[0]['usuario'], $Cliente[0]['password']);
    
    if ($Cliente){
        return $ret;
    } else {
        return "No existe. Probablemente lo has borrado antes";
    }
}

/**
 * Leer el usuario a partir del id del usuario
 *
 * @param [string] $usr Id del usuario del que queremos conocer el valor usuario
 * @return [string] $value->getUsuario();
 */
function ClienteReadUser($usr){
    $arr=array($usr);
    $Cliente = consulta("SELECT * FROM cliente WHERE id=?",$arr);
    $ret=$Cliente[0]['usuario'];
    if ($Cliente){
        return $ret;
    } else {
        return "No existe";
    }
}

/**
 * Actualizar cliente a partir de un objeto cliente
 *
 * @param [object] $Cliente
 * @return [string] Errores en la función
 */
function ClienteUpdate($Cliente){
    $ind = comprobar_id_cliente ();
    //var_dump(in_array((int)$Cliente -> getId(), $ind, true));
    if (in_array((int)$Cliente -> getId(), $ind, true)) {
         $arr = array($Cliente -> getId(), $Cliente -> getNombre(), $Cliente -> getEmail(), $Cliente -> getUsuario(), $Cliente -> getPassword(), $Cliente -> getFecha(),$Cliente -> getId());
        //fputcsv($fp,$arr);
        $query = "UPDATE cliente SET id =?, nombre=?, email=?, usuario=?, password=?, fechanacimiento=? WHERE id=?;";
        $ret = introduce($query,$arr);     
    } else {
        $error['error']= "No puedo actualizar un dato que no existe";
		//header("Location: error.php?error=$error[error]");
    }

}
//$Cliente = new cliente('1','Juanjito','juan@gmail.com','2019-10-23','admin','1234');
//ClienteUpdate($Cliente);
/**
 * Borrar cliente a partir de su id
 *
 * @param [string] $id
 * @return void
 */

function ClienteDelete($id){
    $arr = array($id);
    $query = "DELETE FROM cliente WHERE id=?;";
    $ret = introduce($query,$arr);
}


/*
PEDIDOS
*/
/**
 * Comprobar el id de todos los pedidos
 *
 * @return [array] $ind Nos devueve un array con todos los índices.
 */
function comprobar_id_Pedido () {
    $clie = PedidoReadAll();
    $ind = array();
    foreach ($clie as $value) {
        array_push($ind,$value->getId_pedido());
    }
    return $ind;
}
/**
 * Leemos todos los pedidos
 *
 * @return [array] $Pedidos Array de objetos con todos los pedidos.
 */
function PedidoReadAll() {
    //$fp = fopen("pedidos.csv", "r");
    $query = "SELECT * FROM pedido;";
    $ret = consulta($query,false);
    $Pedidos = [];
    $linea_Pedido = [];
    $i = 0;
    foreach($ret as $linea_Pedido) {
        $Pedidos[$i] = new Pedido ($linea_Pedido['id'],$linea_Pedido['clienteid'],$linea_Pedido['fecha']);
        $i++;
    }
    return $Pedidos;
    //fclose($fp); 
}

/**
 * Crear pedido a partir de un objeto Pedido
 *
 * @param [object] $Pedido
 * @return [string] Errores generados
 */
function PedidoCreate($Pedido) {
    $ind = comprobar_id_Pedido ();
    
    if (in_array($Pedido -> getId_pedido(), $ind, true)) {
        echo "Se encontró indice repetido. El próximo id libre en el contador es ".(max($ind)+1).".\n";
    } else {
        $arr = array($Pedido -> getId_pedido(), $Pedido -> getId_cliente(), $Pedido -> getFecha());
        $query = "INSERT INTO pedido (id,clienteid,fecha) VALUES (?,?,?);";
        $ret = introduce($query,$arr);
    }    
}


/**
 * Leemos un pedido a partir del id
 *
 * @param [string] $id
 * @return [string] Errores de la función
 */
function PedidoRead($id){
    $arr=array($id);
    $Pedido = consulta("SELECT * FROM pedido WHERE id=?",$arr);
    $ret=new pedido ($Pedido[0]['id'], $Pedido[0]['clienteid'], $Pedido[0]['fecha']);
    
    if ($Pedido){
        return $ret;
    } else {
        return "No existe";
    }
}

//$a = PedidoReadAll();
//var_dump($a);
/**
 * Actualizamos el listado de Pedidos
 *
 * @param [object] $Pedido
 * @return void
 */
function PedidoUpdate($Pedido){
    $ind = comprobar_id_Pedido ();
    if (in_array($Pedido -> getId(), $ind, true)) {
        $arr = array($Pedido -> getIdPedido(), $Pedido -> getIdCLiente(), $Pedido -> getFecha(),$Pedido -> getIdPedido());
        //fputcsv($fp,$arr);
        $query = "UPDATE pedido SET id =?, clienteid=?, fecha=? WHERE  id=?;";
        $ret = introduce($query,$arr);       
    } else {
        $error['error']= "No puedo actualizar un dato que no existe";
		header("Location: error.php?error=$error[error]");
    }
    
}
/**
 * Borramos Pedido a partir del id
 *
 * @param [type] $id
 * @return [string] Errores de la función
 */
function PedidoDelete($id){
    $arr=array($id);
    $query = "DELETE FROM pedido WHERE id=?;";
    $ret = introduce($query,$arr);
}

/*
LINEAS
*/
/**
 * Comprobamos los num de lineas en funcion del pedido
 *
 * @param [string] $ped Id del pedido
 * @return [array] $ind listado de todos los numeros de linea
 */
function comprobar_id_Lineas ($ped) {
    $lin = LineasReadAll();
    $ind = [];
    foreach ($lin as $value) {
        if ($value -> getId_pedido() == $ped){
            array_push($ind,$value->getNum_linea());
        }
    }
    return $ind;
}
/**
 * Leemos todas las lineas del archivo de lineas
 *
 * @return [array] $Lineas array de objetos lineas
 */
function LineasReadAll() { 
    $query = "SELECT * FROM linea;";
    $ret = consulta($query,false);
    $Lineas = [];
    $linea_Lineas = [];
    $i = 0;
    foreach($ret as $linea_Lineas) {
        $Lineas[$i] = new lineas ($linea_Lineas['numero'], $linea_Lineas['articuloid'], $linea_Lineas['nombre'],$linea_Lineas['precio'],$linea_Lineas['pedidoid']);
        $i++;
        //$num_linea, $id_articulo, $nombre, $precio, $id_pedido
    }
    return $Lineas;
    
}

/**
 * Creamos Lineas a partir de un objeto lineas
 *
 * @param [object] $Lineas Objeto a añadir
 * @return void
 */
function LineasCreate($Lineas) {
    //$fp = fopen("lineas.csv", "a");
    $arr = array($Lineas -> getNum_linea(), $Lineas -> getId_articulo(), $Lineas -> getNombre(), $Lineas -> getPrecio(), $Lineas -> getId_pedido());
    $query = "INSERT INTO linea (numero,articuloid,nombre,precio,pedidoid) VALUES (?,?,?,?,?);";
    $ret = introduce($query,$arr);

}
/**
 * Devuelve array de objetos lineas con la del pedido.
 *
 * @param [srring] $id_pedido Id del pedido del que queremos conocer las lineas
 * @return [srring] $todas_las_lineas_pedido Todas las lineas del pedido.
 */
function LineasRead($id_pedido){
    $Lineass = LineasReadAll();
    $todas_las_lineas_pedido = [];
    foreach ($Lineass as $value) {
        if ($value -> getId_pedido() == $id_pedido){
            array_push($todas_las_lineas_pedido, $value);
        }
    }
    if($todas_las_lineas_pedido) {
        return $todas_las_lineas_pedido;
    } else {
        return "Sin datos";
    }
}
/**
 * Actualizamos las Lineas
 *
 * @param [object] $Lineas
 * @return [string] Errores
 */
function LineasUpdate($Lineas){
    $arr = array($Lineas -> getNombre(),$Lineas -> getPrecio(),$Lineas -> getId_articulo(),$Lineas -> getNum_linea(),$Lineas -> getId_pedido());
    $query = "UPDATE linea SET nombre=?,precio=?, articuloid=? WHERE numero=? AND pedidoid=?";
    introduce($query,$arr); 
    }
/**
 * Borramos Líneas, necesitamos el numero de linea y el pedido al cual pertenece
 *
 * @param [string] $id_pedido
 * @param [string] $num_linea
 * @return void
 */
function LineasDelete($id_pedido,$num_linea){
    $arr=array($id_pedido,$num_linea);
    $query = "DELETE FROM linea WHERE pedidoid=? AND numero=?;";
    $ret = introduce($query,$arr);
}

/* LOGIN */
/**
 * Comprobamos el usuario y contraseña de la página login
 *
 * @param [string] $usuario
 * @param [string] $password
 * @return [string] Mensajes de error u OK
 */
function login_usuario ($usuario,$password){
    $arr=array($usuario,$password);
    $query = "SELECT id FROM cliente WHERE usuario=? AND password=?;";
    $ret = consulta($query,$arr);
    //var_dump($ret);
    if($ret){
        return $ret[0]['id'];
    }else{
        return "El usuario ". $usuario ." no existe";
    }
}

//var_dump(login_usuario ("lliurex","lliurex"));
/**
 * Leemos el usuario que está logado actualmente
 *
 * @return void
 */
function leer_usr () {
    return $_SESSION["idCliente"];
}








?>
