<?php
/**
* @file listado_pedidos.php
* @brief Saca el listado de pedidos
*
* Hace peticion para mostrar todos los pedidos.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 31/12/2019
*/
session_start();
setcookie(session_name(),session_id(),time()+600);
if(!$_SESSION["idCliente"]){
        header("Location: login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>

<body>
    <h1>Listado Pedidos</h1>
    <h2>Menu</h2>
    <div>

            <a href="index.php">Volver</a><br>
            <a href="pedidos.php">Nuevo</a><br/><br/>

<?php

if($_SESSION['datos']=='csv'){
    require_once("fichero.php");
} elseif ($_SESSION['datos']=='bd') {
    require_once("postgres.php");
}
/**
 * Recogemos todos los art'iculos
 */
$result =  PedidoReadAll();
//var_dump($result);
echo "<table border='1'>";
echo "<tr><td>Id Pedido</td><td>Id Cliente</td><td>Fecha</td><td>Ver</td><td>Actualizar</td><td>Borrar</td></tr>";
//Recorremos todos los articulos para crear una tabla con todos ellos.
foreach ($result as $value) {
    echo "<tr>";
    echo "<td>".$value->getId_pedido()."</td><td>".$value->getId_cliente()."</td><td>".$value->getFecha()."</td></td><td><a href='ver_pedido.php?id=".$value->getId_pedido()."'>Ver</a>
</td><td><a href='actualizar_pedido.php?idPedido=".$value->getId_pedido()."&idCliente=".$value->getId_cliente()."&Fecha=".$value->getFecha()."'>Actualizar</a>
</td><td><a href='control.php?action=borrar_pedido&id=".$value->getId_pedido()."'>Borrar</a>
</td>";
    echo "</tr>";
}
echo "</table>";



?>


    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
