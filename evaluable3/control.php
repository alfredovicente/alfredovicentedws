<?php
/**
* @file control.php
* @brief Archivo que recibe todas las peticiones y que se conecta con fichero.php para devolver la petición.
*
* Este archivo es el que controla las peticiones que se reciben desde los diferentes formularios
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 31/12/2019
*/

use phpDocumentor\Reflection\Location;
session_start();

if (isset($_GET["action"])){
	$action=$_GET["action"];
} elseif (isset($_POST["action"])) {
	$action=$_POST["action"];
} else {
	$error[error]="Falta el parámetro de action";
    header("Location: nuevo.php?error=$error[error]");
}

if (isset($_POST['datos']) || isset($_SESSION['datos'])){
    if ($_POST['datos']=="Postgres remoto" || $_SESSION['datos']=='bd'){
        require("postgres.php");
        session_start();
        if (!isset($_SESSION['datos'])){
            session_destroy();
            session_start();
            $_SESSION['datos']='bd';
        }
        if (!isset($_SESSION['/imgbd/'])){
            $_SESSION['img']='/imgbd/';
        }
    } elseif ($_POST['datos']=="csv" || $_SESSION['datos']=='csv') {
        require("fichero.php");
        session_start();
        if (!isset($_SESSION['datos'])){
            session_destroy();
            session_start();
            $_SESSION['datos']='csv';
        }
        if (!isset($_SESSION['/img/'])){
            $_SESSION['img']='/img/';
        }
    }
}
/**
 * Dependiendo de la acción hacemos una consulta. Cada tipo de consulta se conecta con el archivo fichero.php
 * y hace la petición que necesita.
 */
switch ($action) {
  case 'borrar':
	$val = $_GET["id"];
	ArticuloDelete($val);
	unlink($_SESSION['img'].'/'.$val.'.png');
	header('Location: articulos.php');
    
	break;
  case 'borrar_linea':
	$ped = $_GET["idPedido"];
	$lin = $_GET["num_Linea"];
	LineasDelete($ped,$lin);
	header('Location: actualizar_lineas.php?idPedido='.$ped);	
	break;
  case 'borrar_pedido':
		$val = $_GET["id"];
		PedidoDelete($val);
		$result = LineasRead($val); 
		$i = 0;
		for ($i=0; $i < sizeof($result); $i++) { 
			LineasDelete($val,$i);
		}
		header('Location: listado_pedidos.php');
		break;
  case 'compra':
	    unset($_POST['action']);
		var_dump($_SESSION);
        $text = "";
		$ind = comprobar_id_Pedido();
		if ($ind){
				$new_id = max($ind)+1;
			} else {
				$new_id = 0;
			}
		$hoy = getdate();
		$usuario_logado = ClienteReadUser(leer_usr());
		if($usuario_logado == "No existe"){
			header("Location: error.php?error=".urlencode("Has borrado el usuario logado, vuelvete a logar con un usuario válido"));
			exit();
		}
		$ped = new Pedido ($new_id,leer_usr(),($hoy['year']."-".$hoy['mon']."-".$hoy['mday']));
		PedidoCreate($ped);
		
		$text = "El id de tu pedido es $new_id realizado el dia ".($hoy['mday']."/".$hoy['mon']."/".$hoy['year'])." por el usuario ".$usuario_logado."<br><br><br><table border='1'><tr><th>Id articulo</th><th>Nombre</th><th>Precio</th></tr><tr>";
		foreach ($_POST as $key => $value) {
			$num_linea = comprobar_id_Lineas($new_id);
			//var_dump($num_linea);
			if ($num_linea){
				$new_id_lin = max($num_linea)+1;
			} else {
				$new_id_lin = 0;
			}
			$id_articulo = $value;
			$art = ArticuloRead($id_articulo);
			$nombre = $art -> getNombre();
			$precio = $art -> getPrecio();
			$linea = new lineas ($new_id_lin, $id_articulo, $nombre, $precio, $new_id);
			LineasCreate($linea);
			$text=$text."<tr><td>".$id_articulo."</td><td>".$nombre."</td><td>".$precio."</td></tr>";
			
		}
		$text=$text."</table>";
		header("Location: pedidos.php?text=".urlencode($text));
    break;
  case 'actualizar':
  	$val = new articulos($_POST["idArticulo"],$_POST["nombre"],$_POST["precio"]);
	ArticuloUpdate($val);
	//var_dump($_SERVER);
	$dir_subida = $_SERVER["DOCUMENT_ROOT"]."/ev3/evaluable3/".$_SESSION['img'];
	if ( isset( $_POST['submit'] ) ) print_r( $_FILES );
	if (isset($_FILES['imagen'])){
		$fichero_subido = $dir_subida . $_POST['idArticulo'] . ".png";
		if (pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION) != "png")
		{
			$error['error']= "Extension no valida. Solo vale png.";
			header("Location: error.php?error=$error[error]");
		} else {
			if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
				header("Location: actualizar.php?idArticulo=$_POST[idArticulo]&nombre=$_POST[nombre]&precio=$_POST[precio]&actualizado=si");
			} else {
				$error['error']= "¡Error en subida de ficheros!\n";
				header("Location: error.php?error=$error[error]");
			}
		}
	
	}
	break;
  case 'actualizar_pedido':
	  $val = new Pedido($_POST["idPedido"],$_POST["idCliente"],$_POST["fecha"]);
	  PedidoUpdate($val);
	  header("Location: actualizar_pedido.php?idPedido=$_POST[idPedido]&idCliente=$_POST[idCliente]&Fecha=$_POST[fecha]&actualizado=si");
	  break;
	case 'actualizar_linea':
		$val = new lineas($_POST["num_linea"],$_POST["id_articulo"],$_POST["nombre"],$_POST["precio"],$_POST["id_pedido"]);
		LineasUpdate($val);
		header('Location: actualizar_lineas.php?idPedido='.$_POST["id_pedido"]);
	break;
  case 'nuevo':
	  $val = new articulos($_POST["idArticulo"],$_POST["nombre"],$_POST["precio"]);
	  ArticuloCreate($val);
	  //var_dump($_SERVER);
	  $dir_subida = $_SERVER["DOCUMENT_ROOT"]."/ev3/evaluable3/".$_SESSION['img'];
	  //var_dump ($_FILES["imagen"]);
	  if ( isset( $_POST['submit'] ) ) print_r( $_FILES );
	  if (isset($_FILES['imagen'])){
		  $fichero_subido = $dir_subida . $_POST['idArticulo'] . ".png";
		  if (pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION) != "png")
		  {
			  $error['error']= "Extension no valida. Solo vale png.";
			  header("Location: error.php?error=$error[error]");
		  } else {
			  if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
				  header("Location: actualizar.php?idArticulo=$_POST[idArticulo]&nombre=$_POST[nombre]&precio=$_POST[precio]&actualizado=si");
			  } else {
				  $error['error']= "¡Error en subida de ficheros!\n";
				  //header("Location: error.php?error=$error[error]");
			  }
		  }
	  
	  }
	  break;

  case 'actualizar_clientes':
  	$val=new cliente($_POST['idCliente'],$_POST['Nombre'],$_POST['Email'],$_POST['FechaNacimiento'],$_POST['Usuari'],$_POST['password']);
	ClienteUpdate($val);
	unset($_POST['action']);
	$text = "";
	foreach ($_POST as $key => $value) {
		$text = $text.$key."=".$value."&";
	}
	header("Location: actualizar_clientes.php?$text"."actualizado=si");


	break;
case 'borrar_clientes':
	ClienteDelete($_GET["idCliente"]);	
	header('Location: clientes.php'); 
    break;

case 'nuevo_clientes':
	  //var_dump($_POST);
	  $cliente = new Cliente($_POST['idCliente'],$_POST['Nombre'],$_POST['Email'],$_POST['FechaNacimiento'],$_POST['Usuari'],$_POST['Password']);
	  $error['err'] = ClienteCreate($cliente);
	if ($error['err'] != "OK"){
		header("Location: error.php?error=$error[err]");
	} else {
		header("Location: nuevo_cliente.php?id=".($_POST['idCliente']+1)."&anadido=si");
	}
    break;
        
case 'salir':
        $_SESSION=array();// VACIAMOS LA VARIABLE DE SESION
        session_unset();// HACEMOS QUE LA VARIABLE YA NO ESTE DEFINIDA
        session_destroy();// DESTRUIMOS SESION DE SERVIDOR
        $session_name = session_name();// OBTENEMOS NOMBRE DE COOKIE DE SESION
        if(isset($_COOKIE[$session_name]))// VERIFICAMOS QUE EXISTA LA COOKIE
            {
                setcookie(session_name(),session_id(), time()-3600, '/');
            }// ELIMINANDO COOKIE DE VARIABLE DE SESION 
        header("Location: login.php");
        break;

case 'login':
  	$username = $_POST['Usuario'];
	$password = $_POST['password'];
	$ret = login_usuario($username,$password);
	if ($ret){
        setcookie(session_name(),session_id(),time()+3600);
        $_SESSION["idCliente"] = $ret;
        
        //var_dump($_SESSION);
		header("Location: index.php");
        break;
	} else {
        session_destroy();
    	header("Location: error.php?error=$error[error]");
        break;
	}
	//var_dump($result);  
  	break;
        

        
  default:
  	$error[error]="Falta el parámetro de action o no esta bien definido";
    header("Location: error.php?error=$error[error]");
    break;
}
