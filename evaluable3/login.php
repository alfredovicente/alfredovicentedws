<?php
/**
* @file login.php
* @brief Archivo de login
*
* Formulario de login
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 31/12/2019
*/
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>

<body>
    <h1>Login</h1>
    <form method="post" action="control.php">
    <table border="1">
        <tr>
            <td>Usuario:</td>
            <td><input name="Usuario" type="text" value="lliurex"></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input name="password" type="password" value="lliurex"></td>
        </tr>
        <tr>
            <td>Fuente de datos:</td>
            <td><select name="datos">
                    <option selected value="Postgres remoto">Postgres remoto</option>
                    <option value="csv">csv</option>
                </select>
            </td>            
            
        </tr>
        <input name="action" type="text" value="login" hidden>
        <tr>
            <td>Enviar:</td>
            <td><input name="login" type="submit" value="Enviar"></td>
        </tr>
        </table>
    </form>
    


   <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
