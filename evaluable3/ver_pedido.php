<?php
/**
 * @file ver?pedido.php
* @brief Archivo para ver todos los pedidos
*
* Se crea una tabla para ver en detalle cada uno de los artículos
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 31/12/2019
*/
session_start();
setcookie(session_name(),session_id(),time()+600);
if(!$_SESSION["idCliente"]){
        header("Location: login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>

<body>
    <h1>Pedidos</h1>
    <h2>Menu</h2>
    <div>

            <a href="listado_pedidos.php">Volver</a><br><br>

<?php
        
if($_SESSION['datos']=='csv'){
    require_once("fichero.php");
} elseif ($_SESSION['datos']=='bd') {
    require_once("postgres.php");
}

$val = $_GET['id'];

$result = PedidoRead($val);
$result2 = LineasReadAll();
        
echo "<table border='1'>";
echo "<tr><td>key</td><td>valor</td></tr>";

    echo "<tr>"; 
    echo "<td>Id Pedido</td><td>".$result->getId_pedido()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>Id Cliente</td><td>".$result->getId_cliente()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>Fecha</td><td>".$result->getFecha()."</td>";
    echo "</tr>";

echo "</table>";
echo "<h2>Listado del pedido</h2>";
echo "<table border='1'>";
echo "<tr><td>num_linea</td><td>id_articulo</td><td>nombre</td><td>precio</td><td>id_pedido</td></tr>";
foreach ($result2 as $value) {
    if ($val == $value->getId_pedido()){
        
            echo "<tr>"; 
            echo "<td>".$value->getNum_linea()."</td>";
            echo "<td>".$value->getId_articulo()."</td>";
            echo "<td>".$value->getNombre()."</td>";
            echo "<td>".$value->getPrecio()." €</td>"; 
            echo "<td>".$value->getId_pedido()."</td>";
            echo "</tr>";

    }
}
echo "</table>";



?>


    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
