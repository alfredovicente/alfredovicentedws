<?php
/**
 * @file nuevo_cliente.php
* @brief Archivo de nuevo cliente
*
* Formulario para rellenar un cliente nuevo
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 31/12/2019
*/
session_start();
setcookie(session_name(),session_id(),time()+600);
if(!$_SESSION["idCliente"]){
        header("Location: login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Nuevo Cliente</title>
</head>


<body>
    <h1>Nuevo Cliente</h1>
    <h2>Menu</h2>
    <div>
            
            <a href="clientes.php">Volver</a>
            <br/><br/>
            <form action="control.php" method="post">
                <table border='1'>
                    <?php
                    $text = "";
                    $keys = array('idCliente', 'Nombre', 'Email', 'Usuari', 'Password', 'FechaNacimiento');
                    
					//Funcion que define el tipo de parametro en el input
					function tipo ($val) {
                        switch ($val) {
                            case 'FechaNacimiento': return 'date'; break;
                            case 'idCliente': return 'number'; break;
                            case 'Password': return 'password'; break;
                            case 'Email': return 'mail'; break;
                            default: return 'text'; break;      
                        }
                    }
                    //Creamos todos los inputs de Cliente
                    foreach ($keys as $value) {
                        $tip = tipo($value);
                        if ($value=="idCliente"){
                            echo "<tr><td>$value</td><td><input type='$tip' name='$value' value='$_GET[id]' readonly></td></tr>";
                        } else {
                            echo "<tr><td>$value</td><td><input type='$tip' name='$value'></td></tr>";
                        }
                        $text = $text.$value."=&";
                                                
                    }
                   

                    ?>

                    <input type="text" name="action" value="nuevo_clientes" hidden>

                </table>
                <br>
                <input type="submit" value="Nuevo">
                <input type="button" value="Borrar" onclick="javascript:location.href='nuevo_cliente.php'">
            </form>
            <br>
            <div style='color: red'>
                <?php
                if(isset($_GET['anadido'])){
                    echo "El valor ha sido añadido";
                }
                ?>
            </div>




    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>
