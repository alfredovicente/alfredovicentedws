<?php
/**
 * @file cliente.php
* @brief Clase cliente
*
* Generamos todos los métodos y variables necesarios.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/

class Cliente {
//Propiedades
    private $id;
    private $nombre;
    private $email;
    private $fecha;
    private $usuario;
    //Constructor
    protected $password;

    public function __construct($id, $nombre, $email, $fecha, $usuario, $password){
        $this->id = $id;
        $this->nombre = $nombre;
        $this->email = $email;
        $this->fecha = $fecha;
        $this->usuario = $usuario;        
        $this->password = $password;
    }

    //Metodos
    /**
     * Recibir valor de id
     */ 
    public function getId() {
        return $this->id;
    }
    /**
     * Establecer valor de id
     *
     * @return  self
     */ 
    public function setId($id) {
        $this->id = $id;
    }
    /**
     * Recibir valor de nombre
     */ 
    public function getNombre() {
        return $this->nombre;
    }
    /**
     * Establecer valor de nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre) {
        $this->nombre=$nombre;
    }
    /**
     * Recibir valor de fecha
     */ 
    public function getFecha() {
        return $this->fecha;
    }
    /**
     * Establecer valor de fecha
     *
     * @return  self
     */ 
    public function setFecha($fecha) {
        $this->precio=$fecha;
    }
    /**
     * Recibir valor de email
     */ 
    public function getEmail() {
        return $this->email;
    }
    /**
     * Establecer valor de email
     *
     * @return  self
     */ 
    public function setEmail($email) {
        $this->email=$email;
    }
    /**
     * Recibir valor de usuario
     */ 
    public function getUsuario() {
        return $this->usuario;
    }
    /**
     * Establecer valor de usuario
     *
     * @return  self
     */ 
    public function setUsuario($usuario) {
        $this->usuario=$usuario;
    }

    /**
     * Recibir valor de password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Establecer valor de password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
}
?>
