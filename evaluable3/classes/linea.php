<?php
/**
 * @file linea.php
* @brief Clase cliente
*
* Generamos todos los métodos y variables necesarios.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/
class lineas {
//Propiedades
    private $num_linea;
    private $id_articulo;
    private $nombre;
    private $precio;
    private $id_pedido;
    //Constructor
    public function __construct($num_linea, $id_articulo, $nombre, $precio, $id_pedido) {
        $this->num_linea = $num_linea;
        $this->id_articulo = $id_articulo;
        $this->nombre = $nombre;
        $this->precio = $precio;
        $this->id_pedido = $id_pedido;
    }
    //Metodos
    /**
     * Sacar valor de id_articulo
     */
    public function getId_articulo() {
        return $this->id_articulo;
    }
    /**
     * Establecer valor de id_articulo
     *
     * @return  self
     */
    public function setId_articulo($id) {
        $this->id_articulo = $id;
    }
    /**
     * Sacar valor de nombre
     */
    public function getNombre() {
        return $this->nombre;
    }
    /**
     * Establecer valor de nombre
     *
     * @return  self
     */
    public function setNombre($nombre) {
        $this->nombre=$nombre;
    }
    /**
     * Sacar valor de precio
     */
    public function getPrecio() {
        return $this->precio;
    }
    /**
     * Establecer valor de precio
     *
     * @return  self
     */
    public function setPrecio($precio) {
        $this->precio=$precio;
    }

    /**
     * Sacar valor de num_linea
     */ 
    public function getNum_linea()
    {
        return $this->num_linea;
    }

    /**
     * Establecer valor de num_linea
     *
     * @return  self
     */ 
    public function setNum_linea($num_linea)
    {
        $this->num_linea = $num_linea;

        return $this;
    }

    /**
     * Sacar valor de id_pedido
     */ 
    public function getId_pedido()
    {
        return $this->id_pedido;
    }

    /**
     * Establecer valor de id_pedido
     *
     * @return  self
     */ 
    public function setId_pedido($id_pedido)
    {
        $this->id_pedido = $id_pedido;

        return $this;
    }
}
