<?php
/**
 * @file pedidos.php
* @brief Archivo de pedidos
*
* Formulario rellenar todos los pedidos
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 31/12/2019
*/
session_start();
setcookie(session_name(),session_id(),time()+600);
if(!$_SESSION["idCliente"]){
        header("Location: login.php");
}
    /**
     * Leemos todos los artículos para mostrarlos en pantalla
     */
    if($_SESSION['datos']=='csv'){
    require_once("fichero.php");
    } elseif ($_SESSION['datos']=='bd') {
    require_once("postgres.php");
    }
    $articulos = ArticuloReadAll();
    /**
     * Esta variable rellenará el html que necesitaremos para mostrar todos los archivos
     */
    $text_articulo = "";
    foreach ($articulos as $value) {
        
        $text_articulo = $text_articulo."<option value='".$value->getId()."'>".$value->getNombre()." ".$value->getPrecio()."€</option>";

                                
    }
    /**
     * Rellenaremos los articulos a medida que se vayan comprando la página se llama a sí misma con la datos que vamos añadiendo.
     */
    $text_compra = "<table border='1'><tr><td>IdArticulo</td><td>Nombre</td><td>Precio</td></tr>";
    $send = "";
    $total = 0;
    //var_dump($_GET);
    if(isset($_GET)){
    $j=0;
        foreach ($_GET as $value) {
                for ($i=0; $i < sizeof($articulos); $i++) { 
                    if($articulos[$i]->getId() == $value){
                        $text_compra = $text_compra."<tr><td>".$articulos[$i]->getId()."</td><td>".$articulos[$i]->getNombre()."</td><td>".$articulos[$i]->getPrecio()." €</td></tr>";
                        $send = $send."<input name='articulo$j' value='".$articulos[$i]->getId()."' hidden>";
                        $total=$total+$articulos[$i]->getPrecio();
                        $j++;
                    }
                }                 
        }
        $text_compra = $text_compra."<tr><td></td><td>TOTAL:</td><td>$total €</td></table>";
    }
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Nuevo Pedido</title>
</head>


<body>
    <h1>Pedido</h1>
    <h2>Menu</h2>
    <div>
            
            <a href="index.php">Volver</a>
            <br/><br/>
            <form action="pedidos.php" method="get">
                <table border='1'>
                    <tr>
                        <td>Articulo:</td>
                        <td><select name="articulo"><? echo "$text_articulo"; ?></select></td>
                        <? echo "$send"; ?>
                    </tr>

                </table>
                <br>
                <input type="submit" value="Añadir">
            </form>
            <br>
            <div>
                                   
                        <? if ($text_compra != "") echo "$text_compra"; ?>           

            </div>
            <br>
            <form action="control.php" method="post">
                <? echo "$send"; ?>
                <input name="action" value="compra" hidden>
                <input type="submit" value="Comprar">
                <input type="button" value="Borrar Compra" onclick="javascript:location.href='pedidos.php'">
            </form>
            
            <br>
            <div style='color: red'>
                <?php
                if(isset($_GET['text'])){
                    echo $_GET['text'];
                }
                ?>
            </div>




    </div>
    <hr>
    <div>  
        <p>CEEDCV 2019-20 Alfredo Vicente <?php echo date('d-m-Y h:i'); ?></p>
    </div>

        
</body>

</html>















