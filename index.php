<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
</head>

<body>
    <h1>Evaluable DWS</h1>
    <hr>
    <div>
        <p>Datos Personales:</p>
        <ul>
            <li>Nombre: Alfredo Rafael Vicente Boix</li>
            <li>Email: alviboi@gmail.com</li>
            <li>Curso: 2019-20</li>
            <li>Fecha: <?php echo date('d-m-Y h:i'); ?></li>
        </ul>
        <p>Evaluables:</p>
        <ul>
            <li><a href="evaluable1/index.php">Evaluable1. Formularios - Vectores.</a></li>
            <li><a href="evaluable2/index.php">Evaluable2. Ficheros - Objetos.</a></li>
            <li><a href="evaluable3/index.php">Evaluable3. BBDD - Sesiones.</a></li>
            <li><a href="evaluable4/web/index.php">Evaluable4. MVC - Mashup.</a></li>
        </ul>
        <p>Fuentes:</p>
        <ul>
            <li><a href="https://bitbucket.org/alfredovicente/alfredovicentedws/src/master/">Bitbucket alumno</a></li>
            <li><a href="https://alfredovicentedws.herokuapp.com/">Heroku alumno</a></li>
        </ul>
    </div>
    <hr>
<script type="text/javascript" id="cookieinfo"
	src="//cookieinfoscript.com/js/cookieinfo.min.js">
</script>

</body>

</html>
