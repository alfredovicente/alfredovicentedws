var searchData=
[
  ['getemail',['getEmail',['../class_cliente.html#a2837df0fc1cc65abb83b135594143a0b',1,'Cliente']]],
  ['getfecha',['getFecha',['../class_cliente.html#a389df493a2b8237d8743285a2cd73b72',1,'Cliente\getFecha()'],['../class_pedido.html#a68dc11015b5fac30e13d18284a12a5e1',1,'Pedido\getFecha()']]],
  ['getid',['getId',['../class_cliente.html#a3c371c4e652dbc67c0f3c190f68993c7',1,'Cliente']]],
  ['getid_5farticulo',['getId_articulo',['../classlineas.html#a8d0ff7a4f5ba89a6340dbe0fb86473ae',1,'lineas']]],
  ['getid_5fcliente',['getId_cliente',['../class_pedido.html#a97d5f679ff459d8201e3e63d5acc1d97',1,'Pedido']]],
  ['getid_5fpedido',['getId_pedido',['../classlineas.html#a6c8223e06f7e247e1b306552485c1640',1,'lineas\getId_pedido()'],['../class_pedido.html#a22a1ba4d4d0964e8753d4eb53878c4b9',1,'Pedido\getId_pedido()']]],
  ['getnombre',['getNombre',['../class_cliente.html#aaf76a8706f53f87ff8f23e2c971ef4fa',1,'Cliente\getNombre()'],['../classlineas.html#a947b9a4dd328fad4067fc3a8b96adfbf',1,'lineas\getNombre()']]],
  ['getnum_5flinea',['getNum_linea',['../classlineas.html#a38a461800ed85bdb66d81242ac8af36e',1,'lineas']]],
  ['getpassword',['getPassword',['../class_cliente.html#a8886555203168549df1e0a883f88c0dc',1,'Cliente']]],
  ['getprecio',['getPrecio',['../classlineas.html#a1cf44c9af7d5913846d217f495125401',1,'lineas']]],
  ['getusuario',['getUsuario',['../class_cliente.html#abcbbb4dae8908c8ecb24170db0835b3f',1,'Cliente']]]
];
