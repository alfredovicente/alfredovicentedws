var searchData=
[
  ['setemail',['setEmail',['../class_cliente.html#ae902607ad409f3b4f19782ac36bc1bc5',1,'Cliente']]],
  ['setfecha',['setFecha',['../class_cliente.html#a2d40f4fa946b5afa9a0f668682f0ca5c',1,'Cliente\setFecha()'],['../class_pedido.html#a8098b750b70e9692a70f937a62d5cb7c',1,'Pedido\setFecha()']]],
  ['setid',['setId',['../class_cliente.html#ab1b3be8156b0db600a795acd54642a18',1,'Cliente']]],
  ['setid_5farticulo',['setId_articulo',['../classlineas.html#ab23d01491f4dffbb63efdb4b80f91e9f',1,'lineas']]],
  ['setid_5fcliente',['setId_cliente',['../class_pedido.html#abdec5559a13b16b769b5551ca63943d7',1,'Pedido']]],
  ['setid_5fpedido',['setId_pedido',['../classlineas.html#a47c94beced43d4c173d4f1e77d631f40',1,'lineas\setId_pedido()'],['../class_pedido.html#a049560f06672abfb8d7b221bcf1ee3a3',1,'Pedido\setId_pedido()']]],
  ['setnombre',['setNombre',['../class_cliente.html#a2a9036ca437743c4a9435ac2acc8c8a5',1,'Cliente\setNombre()'],['../classlineas.html#af3526ef50d5b1684d4e27d32bad13f23',1,'lineas\setNombre()']]],
  ['setnum_5flinea',['setNum_linea',['../classlineas.html#a22612e2cf53a38ab47da1894744990d2',1,'lineas']]],
  ['setpassword',['setPassword',['../class_cliente.html#a304058f202fe7c775f4ad61d9905c66c',1,'Cliente']]],
  ['setprecio',['setPrecio',['../classlineas.html#a04235e3ab7402c156da9ab91fd7cbf08',1,'lineas']]],
  ['setusuario',['setUsuario',['../class_cliente.html#ad7967e2324eff51d4fd2b63a4e2aeb26',1,'Cliente']]]
];
