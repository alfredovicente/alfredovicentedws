var searchData=
[
  ['clientecreate',['ClienteCreate',['../_model_8php.html#aae1e39d936535d23ad502704bef74d64',1,'Model.php']]],
  ['clientedelete',['ClienteDelete',['../_model_8php.html#a9d076506c7bd7f806c021219f36ee594',1,'Model.php']]],
  ['clienteread',['ClienteRead',['../_model_8php.html#a40661ee6e696ba8c32f16cb02686427d',1,'Model.php']]],
  ['clientereadall',['ClienteReadAll',['../_model_8php.html#a512ee46330f0c71f734d5825742b94c6',1,'Model.php']]],
  ['clientereaduser',['ClienteReadUser',['../_model_8php.html#a6f82c5dc580157cd02697c2b6a01c64f',1,'Model.php']]],
  ['clienteupdate',['ClienteUpdate',['../_model_8php.html#a7905e6dbf777fbc5f913d75624ec6533',1,'Model.php']]],
  ['comprobar_5fid_5fart',['comprobar_id_art',['../_model_8php.html#a1658ebffac7bdbcf78813b24d103d270',1,'Model.php']]],
  ['comprobar_5fid_5fcliente',['comprobar_id_cliente',['../_model_8php.html#af64c728a9599c908b24aa63cb23c8518',1,'Model.php']]],
  ['comprobar_5fid_5flineas',['comprobar_id_Lineas',['../_model_8php.html#aa53f50d4a0dfab7033df28b0557f2e52',1,'Model.php']]],
  ['comprobar_5fid_5fpedido',['comprobar_id_Pedido',['../_model_8php.html#a1b91b25c7e3fa0b9b14324253abb2662',1,'Model.php']]],
  ['comprobar_5fusuario',['comprobar_usuario',['../_model_8php.html#a91d5fbc8aaaf89c5ea5b7d2f2a5a967d',1,'Model.php']]]
];
