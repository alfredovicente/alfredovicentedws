<?php
/**
* @file listar_articulos.php
* @brief Template donde se listan los articulos y hacemos el volcado de los ficheros creados
*
*  En primer lugar se han de crear los ficheros con los iconos ya que el volcado se hace directamente de los ficheros.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 15/02/2020
*/
?>
<?php ob_start() ?>

<div id="menu">
    <span>Articulos: </span>
    <a href="index.php?ctl=listar">Listar</a>
    <a href="index.php?ctl=nuevo">Nuevo</a>
    <a href="index.php?ctl=buscar">Buscar</a>
    <a href="index.php?ctl=verJSON">VerJSON</a>
    <a href="index.php?ctl=verRSS">VerRSS</a>
    <hr />
</div>
<h1>Mostrando Articulos</h1>

<?php
    echo "<table border='1'>";
echo "<tr><td>Id</td><td>Nombre</td><td>Precio</td><td>Imagen</td><td>Ver</td><td>Actualizar</td><td>Borrar</td></tr>";
//Recorremos todos los articulos para crear una tabla con todos ellos.
$img = "/img/";
foreach ($result as $value) {
    echo "<tr>";
    echo "<td>".$value->getId()."</td><td>".$value->getNombre()."</td><td>".$value->getPrecio()."</td><td><a href='.".$img.$value->getId().".png' target='_blank'>Imagen</a>
    </td><td><a href='index.php?ctl=ver&id=".$value->getId()."'>Ver</a>
    </td><td><a href='index.php?ctl=actualizar&idArticulo=".$value->getId()."&nombre=".$value->getNombre()."&precio=".$value->getPrecio()."'>Actualizar</a>
    </td><td><a href='index.php?ctl=borrar&id=".$value->getId()."'>Borrar</a>
    </td>";
    echo "</tr>";
}
echo "</table>";
?>
<br>
<div>
    <a href="index.php?ctl=ver_json"><img style="height: 40px; width: 40px;" src="./img/json.png"></a>
    <a href="index.php?ctl=ver_rss"><img style="height: 40px; width: 40px;" src="./img/rss.png"></a>
    <a href="index.php?ctl=ver_csv"><img style="height: 40px; width: 40px;" src="./img/csv.png"></a>

</div>


<?php $contenido = ob_get_clean();
if($most=='json'){
    $file = '../ficheros/articulos.json';
    if(file_exists($file)){
        $show=file_get_contents($file);
        print_r($show); 
    } 
}else if ($most=='rss'){
    $file = '../ficheros/articulos.rss';
    if(file_exists($file)){
        $show=htmlspecialchars(file_get_contents($file));
        print_r($show); 
    }


} ?>

<?php include 'layout.php' ?>