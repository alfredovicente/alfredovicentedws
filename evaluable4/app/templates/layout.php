<?php
/**
* @file layout.php
* @brief És la parte común a la mayoría de los templates
*
*  Aquí se encuentra el menú.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 15/02/2020
*/
?>
<html>

<head>
    <title>Tienda</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="<?php echo 'css/'.Config::$mvc_vis_css ?>" />
</head>
<body>
    <div id="cabecera">
        <h1>Tienda</h1>
    </div>
    <div id="menu">
        <hr />
        <a href="index.php?ctl=inicio">Inicio</a>
        <a href="index.php?ctl=articulos">Articulos</a>
        <a href="index.php?ctl=mashup">Mashup</a>
        <a href="../../index.php">Salir</a>
        <hr />
    </div>
    <div id="contenido">
        <?php echo $contenido ?>
    </div>
    <div id="pie">
        <hr />
        <div>- CEED DWS EVALUABLE 4 -</div>
    </div>
</body>

</html>
