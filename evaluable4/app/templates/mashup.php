<?php
/**
* @file mashup.php
* @brief Template donde podemos encontrar el formulario de búsqueda de New York Times
*
*  Nos hemos dado de alta en la API de New York Times para poder hacer peticiones directamente. El resultado se limita a 10 resultados a no   ser que pagues.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 15/02/2020
*/
?>
<?php ob_start();

//var_dump($resultado); 
//var_dump($url);

?>

<h1>Busca articulos en el New York Times</h1>
<form name="formBusqueda" action="index.php?ctl=mashup_busca" method="POST">
    <table>
        <tr>
            <td>Texto a buscar (en inglés):</td>
            <td><input type="text" name="texto" value="<?php $texto ?>" required></td>
            <td>Busca desde:</td>
            <td><input type="date" name="fecha_de" value="<?php $fecha_de ?>" required></td>
            <td>Busca hasta:</td>
            <td><input type="date" name="fecha_hasta" value="<?php $fecha_hasta ?>" required></td>
            <td><input type="submit" value="buscar"></td>
        </tr>
    </table>
</form>
<?php if ($resultado != ''):?>
<table>
    <?php foreach ($resultado->response->docs as $item) : ?>
    <table><tr><td>
    <a href="<?php echo $item->web_url ?>"><?php echo $item->snippet ?></a><br><br>
    </td></tr></table>
    <?php endforeach; ?>
</table>
<?php endif; ?>

<?php $contenido = ob_get_clean() ?>
<?php include 'layout.php' ?>