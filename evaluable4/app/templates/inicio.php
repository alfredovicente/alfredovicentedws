<?php
/**
* @file inicio.php
* @brief Template donde tenemos los enlaces para bajar el enunciado
*
* Template donde tenemos los enlaces para bajar el enunciado
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 15/02/2020
*/
?>
<?php ob_start() ?>
<h1>Inicio</h1>
<?php echo $params['mensaje'] ?>
<h3> Fecha: <?php echo $params['fecha'] ?> </h3>
<a href="../web/DWST09Evaluable4.pdf">Evaluable 4 Enunciado</a><br>
<a href="../doc/html/index.html">Documentación</a>

<?php $contenido = ob_get_clean() ?>
<?php include 'layout.php' ?>