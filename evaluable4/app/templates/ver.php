<?php
/**
 * @file ver.php
* @brief Archivo para ver todos los artículos
*
* Se crea una tabla para ver en detalle cada uno de los artículos
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 15/02/2020
*/
?>
<?php ob_start() ?>

<div id="menu">
    <span>Articulos: </span>
    <a href="index.php?ctl=listar">Listar</a>
    <a href="index.php?ctl=nuevo">Nuevo</a>
    <a href="index.php?ctl=buscar">Buscar</a>
    <a href="index.php?ctl=verJSON">VerJSON</a>
    <a href="index.php?ctl=verRSS">VerRSS</a>
    <hr />
</div>
    <h1>Artículos</h1>
    <div>

<?php

echo "<table border='1'>";
echo "<tr><td>key</td><td>valor</td></tr>";

    echo "<tr>"; 
    echo "<td>Id Articulo</td><td>".$result->getId()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>Nombre</td><td>".$result->getNombre()."</td>";
    echo "</tr>";
    echo "<tr>"; 
    echo "<td>Precio</td><td>".$result->getPrecio()."</td>";
    echo "</tr>";

echo "</table>";



?>


<?php $contenido = ob_get_clean() ?>
<?php include 'layout.php' ?>
