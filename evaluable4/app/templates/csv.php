<?php
/**
* @file csv.php
* @brief Template que gestiona la creación y descarga del archivo csv.
*
* Template que gestiona la creación y descarga del archivo csv.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 15/02/2019
*/
?>
<?php
$file = '../ficheros/articulos.csv';
$fp = fopen($file, 'w');
foreach ($result as $fields) {
    if( is_object($fields) )
        $fields = (array) $fields;
    fputcsv($fp, $fields);
}

fclose($fp);
header('Location: '.$file);
?>