<?php
/**
* @file json.php
* @brief Template donde creamos el archivo json
*
*  Template donde creamos el archivo json
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 15/02/2020
*/
?>
<?php
$json_string = json_encode($result);
$file = '../ficheros/articulos.json';
$fp = fopen($file, 'w');
fwrite($fp, $json_string);
ob_clean();
fclose($fp);
header('Content-Type: application/json');
readfile($file);
//header('Location: '.$file);
?>