<?php ob_start() ?>

<div id="menu">
    <span>Articulos: </span>
    <a href="index.php?ctl=listar">Listar</a>
    <a href="index.php?ctl=nuevo">Nuevo</a>
    <a href="index.php?ctl=buscar">Buscar</a>
    <a href="index.php?ctl=verJSON">VerJSON</a>
    <a href="index.php?ctl=verRSS">VerRSS</a>
    <hr />
</div>
<h1>Articulos</h1>
<form name="formBusqueda" action="index.php?ctl=buscar" method="POST">
    <table>
        <tr>
            <td>nombre item:</td>
            <td><input type="text" name="nombre" value="<?php echo $params['nombre']?>">(puedes utilizar '%' como comodín)</td>
            <td><input type="submit" value="buscar"></td>
        </tr>
    </table>
    </table>
</form>
<?php if ($params['resultado'] != ''): ?>
<table>
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Precio</th>
    </tr>
    <?php foreach ($params['resultado'] as $item) : ?>
    
    <tr>
        <td><a href="index.php?ctl=ver&id=<?php echo $item->getId(); ?>">
            <?php echo $item->getId(); ?></a></td>
        <td><?php echo $item->getNombre(); ?></td>
        <td><?php echo $item->getPrecio(); ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php endif; ?>
<?php $contenido = ob_get_clean() ?>
<?php include 'layout.php' ?>
