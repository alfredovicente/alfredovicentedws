<?php
/**
* @file actualizar.php
* @brief Archivo para actualizar Artículo
*
* Formulario que actualiza articulo
*
* @author Alfredo Rafael Vicente Boix
* @version 1.1
* @date 31/12/2019
*/
?>
<?php ob_start() ?>
    <div id="menu">
        <span>Articulos: </span>
        <a href="index.php?ctl=listar">Listar</a>
        <a href="index.php?ctl=nuevo">Nuevo</a>
        <a href="index.php?ctl=buscar">Buscar</a>
        <a href="index.php?ctl=verJSON">VerJSON</a>
        <a href="index.php?ctl=verRSS">VerRSS</a>
        <hr />
    </div>
    <h1>Artículos</h1>
    <div>
            <form action="index.php?ctl=actualizar_articulo" method="post" enctype="multipart/form-data">
            	<table border='1'>
            		<tr>
            			<td>Id</td>
            			<td><input type="number" name="idArticulo" value="<?php echo $id ?>" readonly></td>
            		</tr>
            		<tr>
            			<td>Nombre</td>
            			<td><input type="text" name="nombre" value="<?php echo $nombre ?>" required></td>
            		</tr>
            		<tr>
            			<td>Precio</td>
            			<td><input type="number" name="precio" step="0.1" value="<?php echo $precio ?>" required></td>
					</tr>
					<tr>
            			<td>Imagen</td>
            			<td><input type="file" name="imagen"></td>
            		</tr>
            	</table>
            	<br>
            	<input type="submit" value="Actualizar">
	            <input type="reset" value="Borrar">
            </form>
            <br>
            <div style='color: red'>
    			<?php
				/**
				 * Si se actualiza se pone un aviso
				 */
    			if(isset($_GET['actualizado'])){
					echo "El valor ha sido actualizado";
				}
				?>
    		</div>




    </div>

<?php $contenido = ob_get_clean() ?>
<?php include 'layout.php' ?>