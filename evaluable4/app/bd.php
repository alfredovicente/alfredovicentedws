<?php

/**
 * @file bd.php
* @brief Configuracion de la base de datos
*
* Aquí se guarda la configuración de la base de datos y funciones para acceder a ella.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 31/12/2019
*/


// Conectando y seleccionado la base de datos  
function conn () {
/*     $dbconn = pg_connect("host=ec2-54-75-238-138.eu-west-1.compute.amazonaws.com
 dbname=dcqculpsnak3fp user=wcplrqhuslskka password=5b0afbb3e605fcbe9e8ca5d5c3e8e88e7d995ba00bb1d35dfffbc4509de2a080")
    or die('No se ha podido conectar: ' . pg_last_error());
    return $dbconn;*/
    
    
    $host='ec2-54-75-238-138.eu-west-1.compute.amazonaws.com';
    $db = 'dcqculpsnak3fp';
    $username = 'wcplrqhuslskka';
    $password = '5b0afbb3e605fcbe9e8ca5d5c3e8e88e7d995ba00bb1d35dfffbc4509de2a080';
    $port=5432;
    $dsn = "pgsql:host=$host;port=$port;dbname=$db;user=$username;password=$password;options='--client_encoding=UTF8';";

    try{
     // Crear la conexion a la base de datos postgresql
     $conn2 = new PDO($dsn);

     // Mostrar un mensaje si la conexion es efectiva
     if($conn2){
     //echo "Conexión a la base de datos <strong>$db</strong> Exitosa!";
        return $conn2;
     }
    }catch (PDOException $e){
     // Reportar mensaje de error
     echo $e->getMessage();
    }
    
}


function consulta ($query,$array) {
    $dbconn = conn();
    //$query = 'SELECT * FROM cliente';
    $result = $dbconn->prepare($query);
    
    if ($array){
        $result->execute($array);
        //var_dump($array);
    } else {
        $result->execute();
    }    
    
    //$result = $dbconn->query($query,PDO::FETCH_NUM) or die('La consulta fallo: ');
    $res = $result->fetchAll(PDO::FETCH_ASSOC);
    //$res = pg_fetch_all($result);
    //var_dump($res);
    // Liberando el conjunto de resultados
    //pg_free_result($result);

    // Cerrando la conexión
    $dbconn = null;
    return $res;
} 

function introduce ($query,$array) {
    $dbconn = conn();
    $result = $dbconn->prepare($query);
    //$query = 'SELECT * FROM cliente';
    //$result = $dbconn->prepare($query);
    if ($array != 0){
        $a = $result->execute($array);
        var_dump($a);
    } else {
        $result->execute();
    }
    $dbconn = null;
    /*
    $dbconn = conn();
    pg_query($dbconn, $query);
    pg_close($dbconn);
    */
}


?>