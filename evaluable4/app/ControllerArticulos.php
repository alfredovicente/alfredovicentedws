<?php
/**
* @file ControllerArticulos.php
* @brief Aquí gestionamos las peticiones del menu de articulos
*
* Solo se gestionan las de la parte artículo
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 15/02/2020
*/
?>
<?php


class ControllerArticulos {

    
    /**
     * Nos lleva a la página de listar artículos
     * @brief Nos lleva a la página de listar artículos
     * @return  void
     */

    public function listar_articulos()
    {
        $most='';
        $result = ArticuloReadAll();
        require __DIR__ . '/templates/listar_articulos.php';
    }
    
    /**
     * Nos lleva a la página de actualizar
     * @brief Nos lleva a la página de actualizar
     * @params $is, $precio, $nombre
     * @return  void
     */
    
    public function actualizar($id,$nombre,$precio)
    {
        require __DIR__ . '/templates/actualizar.php';
    }

    /**
     * Esta función gestiona la actualización de un artículo
     * @brief Gestión de la actualización de artículos
     *
     * @return  void
     */
    
    public function actualizar_articulo()
    {
        $val = new articulos($_POST["idArticulo"],$_POST["nombre"],$_POST["precio"]);

        ArticuloUpdate($val);
        //var_dump($_SERVER);
        $dir_subida = $_SERVER["DOCUMENT_ROOT"]."/evaluable4/web/img/";
       if ( isset( $_POST['submit'] ) ) print_r( $_FILES );
        if ($_FILES['imagen']['name'] != ''){
            $fichero_subido = $dir_subida . $_POST['idArticulo'] . ".png";
            if (pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION) != "png")
            {
                $error['error']= "Extension no valida. Solo vale png.";
                header("Location: index.php?ctl=error&error=$error[error]");
            } else {
                if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
                    header("Location: index.php?ctl=actualizar&idArticulo=$_POST[idArticulo]&nombre=$_POST[nombre]&precio=$_POST[precio]&actualizado=si");
                } else {
                    $error['error']= "¡Error en subida de ficheros!\n";
                    header("Location: index.php?ctl=error&error=$error[error]");
                }
            }

        } else {
            header("Location: index.php?ctl=actualizar&idArticulo=$_POST[idArticulo]&nombre=$_POST[nombre]&precio=$_POST[precio]&actualizado=si");
        }
    }
    
    /**
     * Esta función gestiona la visualización de los artículos y los muestra
     * @brief Esta función gestiona la visualización de los artículos y los muestra
     *
     * @return  void
     */
    
    
    public function ver($id)
    {   
        $result = ArticuloRead($id);
        require __DIR__ . '/templates/ver.php';
    }
    
    /**
     * Crea el json y lo muestra
     * @brief Crea el json y lo muestra
     *
     * @return  void
     */
    
    public function ver_json()
    {   
        $result = ArticuloReadAll();
        require __DIR__ . '/templates/json.php';
    }
    
     /**
     * Crea el rss y lo muestra
     * @brief Crea el rss y lo muestra
     *
     * @return  void
     */
    
    public function ver_rss()
    {   
        $result = ArticuloReadAll();
        require __DIR__ . '/templates/rss.php';
    }
    
     /**
     * Crea el csv y lo muestra
     * @brief Crea el csv y lo muestra
     *
     * @return  void
     */
    
    public function ver_csv()
    {   
        $result = ArticuloReadAll();
        require __DIR__ . '/templates/csv.php';
    }
    
     /**
     * Muestra el JSON creado
     * @brief Muestra el JSON creado
     *
     * @return  void
     */
    
    public function verJSON()
    {   
        $result = ArticuloReadAll();
        $most='json';
        require __DIR__ . '/templates/listar_articulos.php';
    }
    
    /**
     * Muestra el RSS creado
     * @brief Muestra el JSON creado
     *
     * @return  void
     */
    
    
    public function verRSS()
    {   
        $result = ArticuloReadAll();
        $most='rss';
        require __DIR__ . '/templates/listar_articulos.php';
    }
    
    /**
     * Busca el artículo con comodín
     * @brief Busca el artículo con comodín
     *
     * @return  void
     */
    
    
    public function buscar()
    {   

        $params = array(
            'nombre' => '',
            'resultado' => array(),
        );

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $params['nombre'] = $_POST['nombre'];
            $params['resultado'] = Buscar_articulo($_POST['nombre']);
        }
        require __DIR__ . '/templates/buscar.php';

    }
    
     /**
     * Añade un nuevo artículo
     * @brief Añade un nuevo artículo
     *
     * @return  void
     */
    
    public function nuevo()
    {       
        $id=max(comprobar_id_art())+1;
        if(isset($_GET['nombre'])){
            $nombre=$_GET['nombre'];
        } else {
            $nombre='';
        }
        if(isset($_GET['precio'])){
            $precio=$_GET['precio'];
        } else {
            $precio='';
        }
        require __DIR__ . '/templates/nuevo.php';

    }

    /**
     * Gestiona los parámetros POST recibidos para añadir un nuevo artículo
     * @brief Gestiona los parámetros POST recibidos para añadir un nuevo artículo
     *
     * @return  void
     */
    
    public function add_nuevo()
    {       
        $id=max(comprobar_id_art())+1;
        $val = new articulos($_POST["idArticulo"],$_POST["nombre"],$_POST["precio"]);
        ArticuloCreate($val);
        //var_dump($_SERVER);
        $dir_subida = $_SERVER["DOCUMENT_ROOT"]."/evaluable4/web/img/";
        //var_dump ($_FILES["imagen"]);
        if ( isset( $_POST['submit'] ) ) print_r( $_FILES );
        if ($_FILES['imagen']['name'] != ''){
            $fichero_subido = $dir_subida . $_POST['idArticulo'] . ".png";
            if (pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION) != "png")
            {
                $error['error']= "Extension no valida. Solo vale png.";
                header("Location: index.php?ctl=error&error=$error[error]");
            } else {
                if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
                    header("Location: index.php?ctl=nuevo&idArticulo=$_POST[idArticulo]&nombre=$_POST[nombre]&precio=$_POST[precio]&actualizado=si");
                } else {
                    $error['error']= "¡Error en subida de ficheros!\n";
                    //header("Location: error.php?error=$error[error]");
                }
            }

        }

    }

    /**
     * Gestiona el borrado de un artículo
     * @brief Gestiona el borrado de un artículo
     *
     * @return  void
     */
    
    
    public function borrar()
    {   
        $val = $_GET["id"];
        ArticuloDelete($val);
        unlink('/ev4/evaluable4/web/img/'.$val.'.png');
        header('Location: index.php?ctl=listar');
    }


}
?>