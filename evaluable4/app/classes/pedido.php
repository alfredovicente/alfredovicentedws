<?php
/**
 * @file pedido.php
* @brief Clase pedido
*
* Generamos todos los métodos y variables necesarios.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/
class Pedido {
    
//Propiedades
    private $id_pedido;
    private $id_cliente;
    private $fecha;
    
    //Constructor
    public function __construct($id_pedido, $id_cliente, $fecha) {
        $this->id_pedido = $id_pedido;
        $this->id_cliente = $id_cliente;
        $this->fecha = $fecha;
    }
    //Metodos
    


    /**
     * Pedimos la variable id_pedido
     */ 
    public function getId_pedido()
    {
        return $this->id_pedido;
    }

    /**
     * Pone valor a la variable id_pedido
     *
     * @return  self
     */ 
    public function setId_pedido($id_pedido)
    {
        $this->id_pedido = $id_pedido;

        return $this;
    }

    /**
     * Pedimos la variable id_cliente
     */ 
    public function getId_cliente()
    {
        return $this->id_cliente;
    }

    /**
     * Pone valor a la variable id_cliente
     *
     * @return  self
     */ 
    public function setId_cliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;

        return $this;
    }

    /**
     * Pedimos la variable fecha
     */ 
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Pone valor a la variable fecha
     *
     * @return  self
     */ 
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }
}
?>