<?php
/**
 * @file articulo.php
* @brief Clase articulo
*
* Generamos todos los métodos y variables necesarios.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 16/11/2019
*/

class articulos implements JsonSerializable {
//Propiedades
    private $id;
    private $nombre;
    private $precio;
    //Constructor
    public function __construct($id, $nombre, $precio) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->precio = $precio;
    }
    //Metodos
    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getNombre() {
        return $this->nombre;
    }
    public function setNombre($nombre) {
        $this->nombre=$nombre;
    }
    public function getPrecio() {
        return $this->precio;
    }
    public function setPrecio($precio) {
        $this->precio=$precio;
    }
    public function jsonSerialize()
    {
        return 
        [
            'id'   => $this->getId(),
            'nombre' => $this->getNombre(),
            'precio' => $this->getPrecio()
        ];
    }
}
?>