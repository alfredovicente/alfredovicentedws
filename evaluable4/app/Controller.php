<?php
/**
* @file Controller.php
* @brief Aquí gestionamos las peticiones del menu principal
*
* Solo se gestionan las del men'u principal
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 15/02/2020
*/
?>

<?php


class Controller {

    /**
     * Nos lleva a la página inicio
     * @brief Nos lleva a la página inicio
     * @return  void
     */

    public function inicio()
    {
        $params = array(
            'mensaje' => 'Alfredo Rafael Vicente Boix. DWS. Curso 2019-2020',
            'fecha' => date('d-m-yyy'),
        );
        require __DIR__ . '/templates/inicio.php';
    }

    /**
     * Nos lleva a la página inicio de la gestión artículos
     * @brief Nos lleva a la página inicio de la gestión artículos
     * @return  void
     */
    public function articulos()
    {
        $params = array(
            'mensaje' => 'Gestion de articulos',
        );
        require __DIR__ . '/templates/articulos.php';
    }
    /**
     * Nos lleva a la página del mashup
     * @brief Nos lleva a la página del mashup
     * @return  void
     */
    public function mashup()
    {
        $resultado="";
        $url="";
        require __DIR__ . '/templates/mashup.php';
    }
    /**
     * Nos lleva a la página de error
     * @brief Nos lleva a la página de error que muestra el error producido
     * @return  void
     */
    public function error()
    {

        require __DIR__ . '/templates/error.php';
    }
}
?>