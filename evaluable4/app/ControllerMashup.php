<?php
/**
* @file ControllerMashup.php
* @brief Aquí gestionamos las peticiones del mashup de New York Times
*
* Se trata de una búsqueda de artículos del New York Times.
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 15/02/2020
*/
?>

<?php


class ControllerMashup {

    /**
     * Gestiona la petición a la api de NYT
     * @brief Gestiona la petición a la api de NYT
     *
     * @return  void
     */
    
    public function mashup_busca()
    {

        if(isset($_POST['texto'])){
            $texto=$_POST['texto'];
        } else {
            $texto='';
        }
        if(isset($_POST['fecha_de'])){
            $fecha_de=str_replace('-','',$_POST['fecha_de']);
        } else {
            $fecha_de='';
        }
        if(isset($_POST['fecha_hasta'])){
            $fecha_hasta=str_replace('-','',$_POST['fecha_hasta']);
        } else {
            $fecha_hasta='';
        }

        $url="https://api.nytimes.com/svc/search/v2/articlesearch.json?fq=".$texto."&begin_date=".$fecha_de."&end_date=".$fecha_hasta."&api-key=T8cvGgEzodB96Cv2WZIuiWlTLb8DOraq";
        $resultado=json_decode(file_get_contents($url));


        //        $resultado=json_decode($url);
        require __DIR__ . '/templates/mashup.php';

    }

}
?>