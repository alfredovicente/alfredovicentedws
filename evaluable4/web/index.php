<?php
/**
* @file index.php
* @brief Archivo que recibe todas las peticiones y se encarga de asociar la peticion con la funcion que la gestiona
*
* Recibe todas las peticiones necesarias
*
* @author Alfredo Rafael Vicente Boix
* @version 1.0
* @date 15/02/2020
*/
/**
 * @mainpage Evaluable 4 
 * <h1>Apartados de la evaluable</h1> 
 *
 
<h1>Datos</h1>

Alumno:Alfredo Rafael Vicente Boix
 
<h1>Evaluable 4</h1>

1. Aplicación funciona según el MVC.

Se ha utilizado el modelo vista controlado, donde tenemos el controlador separado de las vistar y un archivo Model donde se hacen todas las peticiones a la base de datos.

2. Aplicación Artículos. Listar, Buscar, Crear JSON, Crear RSS: 4 puntos.

Se han implementado todas las funcionalidades de los menús. Los enlaces que crean el JSON y el RSS muestran el contenido posteriormente.

3. Aplicación Artículos. Leer JSON, Leer RSS: 2 puntos.

Se lee el JSON creado y el RSS.

4. Aplicación Mashup externa. 1 puntos

Se ha implementado la API del New York Times.

5. Estilos, Documentación, 1 punto.

Se han usado los estilos usados en las prácticas y se ha documentado con Doxygen.

6. Heroku - Postgres Bitbucket. 1 punto

Se han usado los repositorios y se ha hecho el despleigue en heroku.  En la página inicial se encuentra en enlace
 
<h1>Evaluable 3</h1>
 
 1. Base de datos, con eliminación de la inyección de sql a través de consultas preparadas. 
 
SE han usado SQL preparadas que se envían aela base de datos de forma separada a cualquier parámetro. De esta forma es imposible para un atacante inyectar SQL malicioso. Es la forma más recomendable y segura de evitar este tipo de ataques.
 
2. Sesiones y Cookies.

Se ha usado control de sesiones y cookies para delimitar el tiempo de inactividad en una pagina. Las variables de sesion nos han permitido alternar entre acceso a ficheros y acceso a base de datos sin tocar el codigo.

3. Elección del sistema de almacenamiento. Uso de sesiones para almacenar sistema de almacenamiento. Se utilizará un interface llamado Idatos.php con las cabeceras de las funciones de acceso a la fuente de datos.

En lugar de idatos hemos utilizado control.php que se encarga de gestionar las peticiones. Se carga el fichero dependiendo de si hemos escogido base de datos o csv.

4. Presentación, Estilos, Documentación.

Se ha usado doygen para documentar. OJO! Todos los archivos estan documentados. Doxygen se usa para facilitar la lectura de los archivos. Se han usado los mismos formatos de ejemplo.

5. Heroku - Postgres, Bitbucket.

Se han usado los repositorios y se ha hecho el despleigue en heroku. 
 
<h1>Evaluable 2</h1>

1.Realizar el crud de cada fichero punto por cada fichero con las operaciones siguientes. ( 1punto por fichero, total 4 puntos) i.Create 0,5 puntos
2.Read 0,5 puntos
3.Update 0,5 puntosi
4.Delete 0,5 puntos

Todos los Formularios hacen peticiones a un archivo denominado control.php el cual hace las peticiones a los ficheros a través del archivo fichero.php

<h3>fichero.php</h3>

2.Se crean las clases para recoge los datos en objetos leidos en el formulario y operaciones del crud en fichero.php. 
Tendrá 1 punto por fichero. 
Total 4 puntos 

Dentro de la carpeta classes están definidas todas las clases de todos de cada uno de objetos que vamos a necesitar.
 
<h1>Evaluable 1</h1>
3.La aplicación cumple con lo solicitado en la evaluable1 con formularios revisados. 1 punto

En la evaluable 1 se pedía:

1. Formulario 5

Los formularios cumplen con lo que se pedía en la evaluable 1. Se han añadido el uso de imágenes que se pidió por teléfono.

2. Aspecto 1

El aspecto es el pedido y mostrado en imágenes en ambas evaluables.

3. Login 1

Hace login sobre, el id del usuario logado se guarda en un fichero llamado usr.

4. Uso de funciones 1

Se hace uso de funciones en todo momento.

5. Includes y- Configuración 1

Se hace uso de includes y de require en todo momento.

6. Git/Heroku 1

Se ha iniciado el repositorio y se ha subido perfectamente.

Recuerdo que, tal y como comentamos en conversación telefónica. Una vez revisado en esta evaluable 2, todos los puntos me pondrias un 5 en la evaluable1.

4.Presentación, Documentación, Heroku, Bitbucket. 1 punto

Se uso de los repositorios, y se añade esta documentación.

<h1>Objetos</h1>

La idea es que la aplicación pase a utilizar objetos como elemento básico de información.

El elemento de información es en todos momento los objetos. 

* <p/> <br/>
 */
?>

<?php
// web/index.php
// carga del modelo y los controladores
require_once __DIR__ . '/../app/Config.php';
require_once __DIR__ . '/../app/Model.php';
require_once __DIR__ . '/../app/Controller.php';
require_once __DIR__ . '/../app/ControllerArticulos.php';
require_once __DIR__ . '/../app/ControllerMashup.php';
// enrutamiento
$idArticulo='';
$nombre='';
$precio='';
$id='';
$map = array(
    'inicio' => array('controller' =>'Controller', 'action' =>'inicio'),
    'articulos' => array('controller' =>'Controller', 'action' =>'articulos'),
    'error' => array('controller' =>'Controller', 'action' =>'error'),
    'mashup' => array('controller' =>'Controller', 'action' =>'mashup'),
    'insertar' => array('controller' =>'ControllerArticulos', 'action' =>'insertar'),
    'buscar' => array('controller' =>'ControllerArticulos', 'action' =>'buscar'),
    'ver' => array('controller' =>'ControllerArticulos', 'action' =>'ver'),
    'listar' => array('controller' =>'ControllerArticulos', 'action' =>'listar_articulos'),
    'actualizar' => array('controller' =>'ControllerArticulos', 'action' =>'actualizar'),
    'ver' => array('controller' =>'ControllerArticulos', 'action' =>'ver'),
    'ver_json' => array('controller' =>'ControllerArticulos', 'action' =>'ver_json'),
    'ver_rss' => array('controller' =>'ControllerArticulos', 'action' =>'ver_rss'),
    'ver_csv' => array('controller' =>'ControllerArticulos', 'action' =>'ver_csv'),
    'nuevo' => array('controller' =>'ControllerArticulos', 'action' =>'nuevo'),
    'verJSON' => array('controller' =>'ControllerArticulos', 'action' =>'verJSON'),
    'verRSS' => array('controller' =>'ControllerArticulos', 'action' =>'verRSS'),
    'add_nuevo' => array('controller' =>'ControllerArticulos', 'action' =>'add_nuevo'),
    'borrar' => array('controller' =>'ControllerArticulos', 'action' =>'borrar'),
    'actualizar_articulo' => array('controller' =>'ControllerArticulos', 'action' =>'actualizar_articulo'),
    'mashup_busca' => array('controller' =>'ControllerMashup', 'action' =>'mashup_busca'),
    
    
);
// Parseoo análisis de la ruta
if (isset($_GET['ctl'])) {
    if (isset($map[$_GET['ctl']])) {
        $ruta = $_GET['ctl'];
        if ($ruta == "actualizar"){
            $idArticulo=$_GET['idArticulo'];
            $nombre=$_GET['nombre'];
            $precio=$_GET['precio'];
        } else if ($ruta == "ver") {
            $id=$_GET['id'];
        }
    } else {
        header('Status: 404 Not Found');
        echo '<html><body><h1>Error 404: No existe la ruta <i>' .
            $_GET['ctl'] .
            '</p></body></html>';
        exit;
    }
} else {
    $ruta = 'inicio';
}
$controlador = $map[$ruta];
if (method_exists($controlador['controller'],$controlador['action'])) {
    switch ($controlador['action']){
        case 'actualizar': call_user_func_array(array(new $controlador['controller'], $controlador['action']),array($idArticulo,$nombre,$precio)); break;
        case 'ver': call_user_func_array(array(new $controlador['controller'], $controlador['action']),array($id)); break;
        default: call_user_func(array(new $controlador['controller'], $controlador['action']));
    }
} else {
    header('Status: 404 Not Found');
    echo '<html><body><h1>Error 404: El controlador <i>' .
        $controlador['controller'] . '->' .$controlador['action'] .
        '</i> no existe</h1></body></html>';
}
